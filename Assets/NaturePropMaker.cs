﻿using System.Collections;
using System.Collections.Generic;
using Deserialisation;
using UnityEngine;
using WorldToHexagons;

namespace GeometyCreation
{
    class NaturePropMaker : GeometryMakerBehavior
    {

        [SerializeField] private float hexagonRadius = 15; 
        private Dictionary<HexCenterPoint, List<GameObject>> propsByHex = new Dictionary<HexCenterPoint, List<GameObject>>();


        private PropProvider propProvider;

        private void Start()
        {
            CreateParentGroup("Nature");
            propProvider = FindObjectOfType<PropProvider>();
        }

        public override void CreateGeometry(OsmBounds currentBounds, Dictionary<ulong, OsmNode> allNodes, Dictionary<ulong, OsmWay> additionalWays, List<HexCenterPoint> hexCenters)
        {
            
            foreach (HexCenterPoint hexCenter in hexCenters)
            {
                EnsurePlants(hexCenter, currentBounds);
            }
        }

        protected void EnsurePlants(HexCenterPoint hexCenter, OsmBounds currentBounds)
        {
            if (propsByHex.ContainsKey(hexCenter))
            {
                return;
            }
            float plantiness = Random.Range(0, 10); //Todo: plantiness should become part of hex.


            List<GameObject> propsOnHex = new List<GameObject>();
            propsByHex.Add(hexCenter, propsOnHex);

            
            for(int i = 0; i<= plantiness; i++)
            {
                GameObject prop1 = PutPlantInRandomPlace(hexCenter);
                propsOnHex.Add(prop1);
                GameObject prop2 = PutPlantInRandomPlace(hexCenter);
                propsOnHex.Add(prop2);
                GameObject prop3 = PutPlantInRandomPlace(hexCenter);
                propsOnHex.Add(prop3);
                GameObject prop4 = PutPlantInRandomPlace(hexCenter);
                propsOnHex.Add(prop4);
            }
        }

        private GameObject PutPlantInRandomPlace(HexCenterPoint hexCenter)
        {
            GameObject prop;
            if (WaterArea.IsInWater(hexCenter.PolarCoordinates))
            {
                prop = propProvider.GetWaterPlant(hexCenter);
            }
            else
            {
                prop = propProvider.GetGroundPlant(hexCenter);
            }

            Vector3 position = hexCenter.PolarCoordinates.UnitySpace + new Vector3(Random.Range(-hexagonRadius, +hexagonRadius), 0, Random.Range(-hexagonRadius, +hexagonRadius));
            prop.transform.localPosition = position;
            prop.transform.Rotate(Random.Range(0f, 360f) * Vector3.up);
            prop.transform.SetParent(parentGroup);

            return prop;
        }
        
    }
}
