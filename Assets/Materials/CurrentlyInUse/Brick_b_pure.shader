// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Brick_b_pure"
{
	Properties
	{
		_Color0("Color 0", Color) = (0.8088235,0.2854671,0.2854671,0)
		_Color1("Color 1", Color) = (0.6102941,0.5223399,0.3904087,0)
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float3 worldPos;
		};

		uniform float4 _Color0;
		uniform float4 _Color1;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 ase_worldPos = i.worldPos;
			float ifLocalVar3_g3 = 0;
			if( ( sin( ( ase_worldPos * float3( 10,40,20 ) ) ) + float3( 1,1,1 ) ).x <= 1.95 )
				ifLocalVar3_g3 = 1.0;
			else
				ifLocalVar3_g3 = 0.0;
			float ifLocalVar2_g3 = 0;
			if( ( sin( ( ase_worldPos * float3( 10,40,20 ) ) ) + float3( 1,1,1 ) ).y <= 1.95 )
				ifLocalVar2_g3 = 1.0;
			else
				ifLocalVar2_g3 = 0.0;
			float ifLocalVar1_g3 = 0;
			if( ( sin( ( ase_worldPos * float3( 10,40,20 ) ) ) + float3( 1,1,1 ) ).z <= 1.95 )
				ifLocalVar1_g3 = 1.0;
			else
				ifLocalVar1_g3 = 0.0;
			float temp_output_6_0_g3 = ( ifLocalVar3_g3 * ifLocalVar2_g3 * ifLocalVar1_g3 );
			float4 temp_output_20_0_g3 = _Color1;
			float4 ifLocalVar16_g3 = 0;
			if( temp_output_6_0_g3 <= 0.0 )
				ifLocalVar16_g3 = temp_output_20_0_g3;
			else
				ifLocalVar16_g3 = _Color0;
			o.Albedo = ifLocalVar16_g3.rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13801
1927;29;1906;1004;1129.938;719.6755;1.3;True;True
Node;AmplifyShaderEditor.ColorNode;5;-782.4374,-224.3755;Float;False;Property;_Color1;Color 1;1;0;0.6102941,0.5223399,0.3904087,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.WorldPosInputsNode;4;-588,-428;Float;False;0;4;FLOAT3;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;1;-803.7,124.1;Float;False;Property;_Color0;Color 0;0;0;0.8088235,0.2854671,0.2854671,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.FunctionNode;6;-379.0002,-31.29999;Float;False;BrickFunction;-1;;3;9444496ad7471714f8245a1427c24e10;4;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT3;0,0,0;False;3;COLOR;0,0,0,0;False;2;FLOAT;COLOR
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;170,-294;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Brick_b_pure;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;6;0;4;0
WireConnection;6;1;5;0
WireConnection;6;3;1;0
WireConnection;0;0;6;1
ASEEND*/
//CHKSM=89480EF212F5C04D2635DBD9F1422928C86EBF27