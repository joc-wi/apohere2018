// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "GroundTester"
{
	Properties
	{
		_FinestGrainSize("Finest Grain Size", Float) = 0
		_BiggestGrainSize("Biggest Grain Size", Float) = 0
		_ColorA("Color A", Color) = (0.06945022,0.4926471,0.05433606,0)
		_ColorB("Color B", Color) = (0.05103806,0.4264706,0.03762977,0)
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float3 worldPos;
		};

		uniform float _BiggestGrainSize;
		uniform float _FinestGrainSize;
		uniform float4 _ColorA;
		uniform float4 _ColorB;


		float3 mod289( float3 x ) { return x - floor( x / 289.0 ) * 289.0; }

		float4 mod289( float4 x ) { return x - floor( x / 289.0 ) * 289.0; }

		float4 permute( float4 x ) { return mod289( ( x * 34.0 + 1.0 ) * x ); }

		float4 taylorInvSqrt( float4 r ) { return 1.79284291400159 - r * 0.85373472095314; }

		float snoise( float3 v )
		{
			const float2 C = float2( 1.0 / 6.0, 1.0 / 3.0 );
			float3 i = floor( v + dot( v, C.yyy ) );
			float3 x0 = v - i + dot( i, C.xxx );
			float3 g = step( x0.yzx, x0.xyz );
			float3 l = 1.0 - g;
			float3 i1 = min( g.xyz, l.zxy );
			float3 i2 = max( g.xyz, l.zxy );
			float3 x1 = x0 - i1 + C.xxx;
			float3 x2 = x0 - i2 + C.yyy;
			float3 x3 = x0 - 0.5;
			i = mod289( i);
			float4 p = permute( permute( permute( i.z + float4( 0.0, i1.z, i2.z, 1.0 ) ) + i.y + float4( 0.0, i1.y, i2.y, 1.0 ) ) + i.x + float4( 0.0, i1.x, i2.x, 1.0 ) );
			float4 j = p - 49.0 * floor( p / 49.0 );  // mod(p,7*7)
			float4 x_ = floor( j / 7.0 );
			float4 y_ = floor( j - 7.0 * x_ );  // mod(j,N)
			float4 x = ( x_ * 2.0 + 0.5 ) / 7.0 - 1.0;
			float4 y = ( y_ * 2.0 + 0.5 ) / 7.0 - 1.0;
			float4 h = 1.0 - abs( x ) - abs( y );
			float4 b0 = float4( x.xy, y.xy );
			float4 b1 = float4( x.zw, y.zw );
			float4 s0 = floor( b0 ) * 2.0 + 1.0;
			float4 s1 = floor( b1 ) * 2.0 + 1.0;
			float4 sh = -step( h, 0.0 );
			float4 a0 = b0.xzyw + s0.xzyw * sh.xxyy;
			float4 a1 = b1.xzyw + s1.xzyw * sh.zzww;
			float3 g0 = float3( a0.xy, h.x );
			float3 g1 = float3( a0.zw, h.y );
			float3 g2 = float3( a1.xy, h.z );
			float3 g3 = float3( a1.zw, h.w );
			float4 norm = taylorInvSqrt( float4( dot( g0, g0 ), dot( g1, g1 ), dot( g2, g2 ), dot( g3, g3 ) ) );
			g0 *= norm.x;
			g1 *= norm.y;
			g2 *= norm.z;
			g3 *= norm.w;
			float4 m = max( 0.6 - float4( dot( x0, x0 ), dot( x1, x1 ), dot( x2, x2 ), dot( x3, x3 ) ), 0.0 );
			m = m* m;
			m = m* m;
			float4 px = float4( dot( x0, g0 ), dot( x1, g1 ), dot( x2, g2 ), dot( x3, g3 ) );
			return 42.0 * dot( m, px);
		}


		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 ase_worldPos = i.worldPos;
			float3 temp_output_8_0_g9 = ase_worldPos;
			float temp_output_15_0_g9 = _BiggestGrainSize;
			float simplePerlin3D5_g9 = snoise( ( temp_output_8_0_g9 / temp_output_15_0_g9 ) );
			float temp_output_10_0_g9 = _FinestGrainSize;
			float simplePerlin3D4_g9 = snoise( ( temp_output_8_0_g9 / sqrt( ( temp_output_15_0_g9 * temp_output_10_0_g9 ) ) ) );
			float simplePerlin3D6_g9 = snoise( ( temp_output_8_0_g9 / temp_output_10_0_g9 ) );
			float4 temp_output_24_0_g9 = _ColorB;
			o.Albedo =  ( ( simplePerlin3D5_g9 + ( simplePerlin3D4_g9 * 0.25 ) + ( simplePerlin3D6_g9 * 0.125 ) ) - 0.0 > 0.0 ? _ColorA : ( simplePerlin3D5_g9 + ( simplePerlin3D4_g9 * 0.25 ) + ( simplePerlin3D6_g9 * 0.125 ) ) - 0.0 <= 0.0 && ( simplePerlin3D5_g9 + ( simplePerlin3D4_g9 * 0.25 ) + ( simplePerlin3D6_g9 * 0.125 ) ) + 0.0 >= 0.0 ? temp_output_24_0_g9 : temp_output_24_0_g9 ) .rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13801
1927;29;1906;1004;1027.479;514.4432;1;True;True
Node;AmplifyShaderEditor.ColorNode;24;-735.5027,-193.3139;Float;False;Property;_ColorB;Color B;3;0;0.05103806,0.4264706,0.03762977,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;23;-718.6035,-413.014;Float;False;Property;_ColorA;Color A;2;0;0.06945022,0.4926471,0.05433606,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;19;-707.7028,26.38605;Float;False;Constant;_balance;balance;0;0;0;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;28;-737.479,159.5568;Float;False;Property;_FinestGrainSize;Finest Grain Size;0;0;0;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;27;-740.479,248.5568;Float;False;Property;_BiggestGrainSize;Biggest Grain Size;1;0;0;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.FunctionNode;25;-386.7028,5.586029;Float;False;JocsNoise;-1;;9;fd0dae7a91f68f242bd47f05f72f2e47;6;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT3;0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;347.1,-33.8;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;GroundTester;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;25;0;23;0
WireConnection;25;1;24;0
WireConnection;25;2;19;0
WireConnection;25;3;28;0
WireConnection;25;4;27;0
WireConnection;0;0;25;0
ASEEND*/
//CHKSM=2FDDDE9E7E2E8F36981DA8F5116347544DA7E252