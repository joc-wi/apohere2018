// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Bricks_c_oriented"
{
	Properties
	{
		_BrickLength("BrickLength", Range( 0 , 10)) = 0.4
		_BrickHeight("BrickHeight", Range( 0 , 10)) = 0.2
		_WallDirectionNormalized("WallDirectionNormalized", Vector) = (0.707,0,0.707,0)
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float3 worldPos;
		};

		uniform float _BrickLength;
		uniform float3 _WallDirectionNormalized;
		uniform float _BrickHeight;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 ase_worldPos = i.worldPos;
			float3 temp_output_44_0_g19 = ase_worldPos;
			float temp_output_59_0_g19 = ( sin( ( ( ( 2.0 * UNITY_PI ) / _BrickLength ) * ( ( temp_output_44_0_g19 * _WallDirectionNormalized ).x + ( temp_output_44_0_g19 * _WallDirectionNormalized ).y + ( temp_output_44_0_g19 * _WallDirectionNormalized ).z ) ) ) + 1.0 );
			float ifLocalVar40_g19 = 0;
			if( temp_output_59_0_g19 <= 1.95 )
				ifLocalVar40_g19 = 1.0;
			else
				ifLocalVar40_g19 = 0.0;
			float ifLocalVar38_g19 = 0;
			if( ( sin( ( temp_output_44_0_g19.y * ( ( 2.0 * UNITY_PI ) / _BrickHeight ) ) ) + 1.0 ) <= 1.95 )
				ifLocalVar38_g19 = 1.0;
			else
				ifLocalVar38_g19 = 0.0;
			float ifLocalVar37_g19 = 0;
			if( temp_output_59_0_g19 <= 1.95 )
				ifLocalVar37_g19 = 1.0;
			else
				ifLocalVar37_g19 = 0.0;
			float temp_output_43_0_g19 = ( ifLocalVar40_g19 * ifLocalVar38_g19 * ifLocalVar37_g19 );
			float4 temp_output_51_0_g19 = float4( 0,0,0,0 );
			float4 ifLocalVar47_g19 = 0;
			if( temp_output_43_0_g19 <= 0.0 )
				ifLocalVar47_g19 = temp_output_51_0_g19;
			else
				ifLocalVar47_g19 = float4( 1,1,1,0 );
			o.Albedo = ifLocalVar47_g19.rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13801
1927;29;1906;1004;981;502;1;True;True
Node;AmplifyShaderEditor.Vector3Node;4;-701,-10;Float;False;Property;_WallDirectionNormalized;WallDirectionNormalized;2;0;0.707,0,0.707;0;4;FLOAT3;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;2;-623,-103;Float;False;Property;_BrickLength;BrickLength;0;0;0.4;0;10;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;3;-639,152;Float;False;Property;_BrickHeight;BrickHeight;1;0;0.2;0;10;0;1;FLOAT
Node;AmplifyShaderEditor.FunctionNode;13;-333,-27;Float;False;JocsOrientedBricks;-1;;19;5c40c2d9a09484943b1127f11ed51d86;6;0;FLOAT;0.0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;COLOR;0,0,0,0;False;4;FLOAT;0.0;False;5;COLOR;0,0,0,0;False;2;FLOAT;COLOR
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;238,-38;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Bricks_c_oriented;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;13;0;2;0
WireConnection;13;1;4;0
WireConnection;13;4;3;0
WireConnection;0;0;13;1
ASEEND*/
//CHKSM=5E522F943E23EBF486D3011BC05D3F46663142E1