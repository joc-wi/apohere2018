// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Bricks_d_plastered"
{
	Properties
	{
		_BrickLength("BrickLength", Range( 0 , 10)) = 0.4
		_BrickHeight("BrickHeight", Range( 0 , 10)) = 0.2
		_WallDirectionNormalized("WallDirectionNormalized", Vector) = (0.707,0,0.707,0)
		_BrickColor("BrickColor", Color) = (0.4779412,0.2064939,0.1265138,0)
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float3 worldPos;
		};

		uniform float _BrickLength;
		uniform float3 _WallDirectionNormalized;
		uniform float _BrickHeight;
		uniform float4 _BrickColor;


		float3 mod289( float3 x ) { return x - floor( x / 289.0 ) * 289.0; }

		float4 mod289( float4 x ) { return x - floor( x / 289.0 ) * 289.0; }

		float4 permute( float4 x ) { return mod289( ( x * 34.0 + 1.0 ) * x ); }

		float4 taylorInvSqrt( float4 r ) { return 1.79284291400159 - r * 0.85373472095314; }

		float snoise( float3 v )
		{
			const float2 C = float2( 1.0 / 6.0, 1.0 / 3.0 );
			float3 i = floor( v + dot( v, C.yyy ) );
			float3 x0 = v - i + dot( i, C.xxx );
			float3 g = step( x0.yzx, x0.xyz );
			float3 l = 1.0 - g;
			float3 i1 = min( g.xyz, l.zxy );
			float3 i2 = max( g.xyz, l.zxy );
			float3 x1 = x0 - i1 + C.xxx;
			float3 x2 = x0 - i2 + C.yyy;
			float3 x3 = x0 - 0.5;
			i = mod289( i);
			float4 p = permute( permute( permute( i.z + float4( 0.0, i1.z, i2.z, 1.0 ) ) + i.y + float4( 0.0, i1.y, i2.y, 1.0 ) ) + i.x + float4( 0.0, i1.x, i2.x, 1.0 ) );
			float4 j = p - 49.0 * floor( p / 49.0 );  // mod(p,7*7)
			float4 x_ = floor( j / 7.0 );
			float4 y_ = floor( j - 7.0 * x_ );  // mod(j,N)
			float4 x = ( x_ * 2.0 + 0.5 ) / 7.0 - 1.0;
			float4 y = ( y_ * 2.0 + 0.5 ) / 7.0 - 1.0;
			float4 h = 1.0 - abs( x ) - abs( y );
			float4 b0 = float4( x.xy, y.xy );
			float4 b1 = float4( x.zw, y.zw );
			float4 s0 = floor( b0 ) * 2.0 + 1.0;
			float4 s1 = floor( b1 ) * 2.0 + 1.0;
			float4 sh = -step( h, 0.0 );
			float4 a0 = b0.xzyw + s0.xzyw * sh.xxyy;
			float4 a1 = b1.xzyw + s1.xzyw * sh.zzww;
			float3 g0 = float3( a0.xy, h.x );
			float3 g1 = float3( a0.zw, h.y );
			float3 g2 = float3( a1.xy, h.z );
			float3 g3 = float3( a1.zw, h.w );
			float4 norm = taylorInvSqrt( float4( dot( g0, g0 ), dot( g1, g1 ), dot( g2, g2 ), dot( g3, g3 ) ) );
			g0 *= norm.x;
			g1 *= norm.y;
			g2 *= norm.z;
			g3 *= norm.w;
			float4 m = max( 0.6 - float4( dot( x0, x0 ), dot( x1, x1 ), dot( x2, x2 ), dot( x3, x3 ) ), 0.0 );
			m = m* m;
			m = m* m;
			float4 px = float4( dot( x0, g0 ), dot( x1, g1 ), dot( x2, g2 ), dot( x3, g3 ) );
			return 42.0 * dot( m, px);
		}


		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 ase_worldPos = i.worldPos;
			float3 temp_output_8_0_g21 = ase_worldPos;
			float simplePerlin3D5_g21 = snoise( ( temp_output_8_0_g21 / 8.0 ) );
			float simplePerlin3D6_g21 = snoise( ( temp_output_8_0_g21 / 0.5 ) );
			float3 temp_output_44_0_g19 = ase_worldPos;
			float temp_output_59_0_g19 = ( sin( ( ( ( 2.0 * UNITY_PI ) / _BrickLength ) * ( ( temp_output_44_0_g19 * _WallDirectionNormalized ).x + ( temp_output_44_0_g19 * _WallDirectionNormalized ).y + ( temp_output_44_0_g19 * _WallDirectionNormalized ).z ) ) ) + 1.0 );
			float ifLocalVar40_g19 = 0;
			if( temp_output_59_0_g19 <= 1.95 )
				ifLocalVar40_g19 = 1.0;
			else
				ifLocalVar40_g19 = 0.0;
			float ifLocalVar38_g19 = 0;
			if( ( sin( ( temp_output_44_0_g19.y * ( ( 2.0 * UNITY_PI ) / _BrickHeight ) ) ) + 1.0 ) <= 1.95 )
				ifLocalVar38_g19 = 1.0;
			else
				ifLocalVar38_g19 = 0.0;
			float ifLocalVar37_g19 = 0;
			if( temp_output_59_0_g19 <= 1.95 )
				ifLocalVar37_g19 = 1.0;
			else
				ifLocalVar37_g19 = 0.0;
			float temp_output_43_0_g19 = ( ifLocalVar40_g19 * ifLocalVar38_g19 * ifLocalVar37_g19 );
			float4 temp_output_51_0_g19 = float4(0.5735294,0.5735294,0.5735294,0);
			float4 ifLocalVar47_g19 = 0;
			if( temp_output_43_0_g19 <= 0.0 )
				ifLocalVar47_g19 = temp_output_51_0_g19;
			else
				ifLocalVar47_g19 = _BrickColor;
			float4 temp_output_24_0_g21 = float4(0.8602941,0.8602941,0.8602941,0);
			o.Albedo =  ( ( simplePerlin3D5_g21 + 0.0 + ( simplePerlin3D6_g21 * 0.125 ) ) - 0.0 > 0.0 ? ifLocalVar47_g19 : ( simplePerlin3D5_g21 + 0.0 + ( simplePerlin3D6_g21 * 0.125 ) ) - 0.0 <= 0.0 && ( simplePerlin3D5_g21 + 0.0 + ( simplePerlin3D6_g21 * 0.125 ) ) + 0.0 >= 0.0 ? temp_output_24_0_g21 : temp_output_24_0_g21 ) .rgb;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13801
1927;29;1906;1004;979;502;1;True;True
Node;AmplifyShaderEditor.ColorNode;18;-698,172;Float;False;Constant;_MortarColor;MortarColor;3;0;0.5735294,0.5735294,0.5735294,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;16;-678,17;Float;False;Property;_BrickColor;BrickColor;3;0;0.4779412,0.2064939,0.1265138,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;3;-704,-236;Float;False;Property;_BrickHeight;BrickHeight;1;0;0.2;0;10;0;1;FLOAT
Node;AmplifyShaderEditor.Vector3Node;4;-700,-146;Float;False;Property;_WallDirectionNormalized;WallDirectionNormalized;2;0;0.707,0,0.707;0;4;FLOAT3;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;2;-700,-329;Float;False;Property;_BrickLength;BrickLength;0;0;0.4;0;10;0;1;FLOAT
Node;AmplifyShaderEditor.ColorNode;19;-666,336;Float;False;Constant;_PlasterColor;PlasterColor;3;0;0.8602941,0.8602941,0.8602941,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.FunctionNode;13;-333,-27;Float;False;JocsOrientedBricks;-1;;19;5c40c2d9a09484943b1127f11ed51d86;6;0;FLOAT;0.0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;COLOR;0,0,0,0;False;4;FLOAT;0.0;False;5;COLOR;0,0,0,0;False;2;FLOAT;COLOR
Node;AmplifyShaderEditor.FunctionNode;14;86,90;Float;False;JocsNoise_2Stages;-1;;21;eb884aad3d1dbb543817d7cdfefe6c1d;6;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT3;0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;443,-29;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Bricks_d_plastered;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;13;0;2;0
WireConnection;13;1;4;0
WireConnection;13;3;18;0
WireConnection;13;4;3;0
WireConnection;13;5;16;0
WireConnection;14;0;13;1
WireConnection;14;1;19;0
WireConnection;0;0;14;0
ASEEND*/
//CHKSM=F009F7B8657DE4B0E15BD41CF7550D526AD57521