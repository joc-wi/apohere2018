// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "OrientedShader"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float3 worldPos;
		};

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 ase_worldPos = i.worldPos;
			float3 temp_cast_0 = (sin( ( ( ase_worldPos * mul( float3(-0.2,0,0.9), float3x3(0,0,1,0,0,0,-1,0,0) ) ).x + ( ase_worldPos * mul( float3(-0.2,0,0.9), float3x3(0,0,1,0,0,0,-1,0,0) ) ).y + ( ase_worldPos * mul( float3(-0.2,0,0.9), float3x3(0,0,1,0,0,0,-1,0,0) ) ).z ) )).xxx;
			o.Albedo = temp_cast_0;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13801
1927;29;1906;1004;951;502;1;True;True
Node;AmplifyShaderEditor.Vector3Node;8;-616,-166;Float;False;Constant;_Vector0;Vector 0;0;0;-0.2,0,0.9;0;4;FLOAT3;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.FunctionNode;7;-385,-158;Float;False;OrientedBricks;-1;;7;5c40c2d9a09484943b1127f11ed51d86;5;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;COLOR;0,0,0,0;False;3;FLOAT3;0,0,0;False;4;COLOR;0,0,0,0;False;3;FLOAT;FLOAT;COLOR
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;79,-137;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;OrientedShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;7;0;8;0
WireConnection;0;0;7;1
ASEEND*/
//CHKSM=2A11954314B86194D337C433F38B4C95C5EEC111