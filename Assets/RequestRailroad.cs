﻿using Deserialisation;
using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WorldToHexagons;

public class RequestRailroad : MonoBehaviour
{


    public static event Action LoadingFailedEvent;

    public static event Action<OsmBounds, Dictionary<ulong, OsmNode>, Dictionary<ulong, OsmWay>, List<HexCenterPoint>> CreatingGeometryEvent;
    public static event Action ContentCreationFinishedEvent;

    [SerializeField][UsedImplicitly]
    private ProvideMapStringBase mapStringProvider;

    private MapData mapData = new MapData();

    /// <summary>
    /// The radius of the requested area, in degree.
    /// </summary>
    [SerializeField]
    private float radius = 0.002f;
    [SerializeField]
    private uint hexGenerations = 18;

    private List<PolarCoordinates> knownAreaCenters = new List<PolarCoordinates>();


    private HexCreator hexCreator = new HexCreator();
    private void Start()
    {
        Debug.Assert(mapStringProvider != null);
    }
    public void RequestNewMapIfNecessary(PolarCoordinates center)
    {
        if (!IsRequestNecessary(center))
        {
            return;
        }
        StartCoroutine(Railroad(center));
    }

    private IEnumerator Railroad(PolarCoordinates center)
    {
        knownAreaCenters.Add(center);

        PolarCoordinates lowerLeft = new PolarCoordinates(center.lat - radius, center.lon - radius);
        PolarCoordinates upperRight = new PolarCoordinates(center.lat + radius, center.lon + radius);
        yield return mapStringProvider.StartGettingMapData(lowerLeft, upperRight, OnMapDataArrived);
    }

    private void OnMapDataArrived(bool successfull, string mapString)
    {
        if (IsAnswerLegal(mapString))
        {
            StartCoroutine(RailroadContinues(mapString));
        }
    }

    private IEnumerator RailroadContinues(string mapString)
    {
        if (!IsAnswerLegal(mapString))
        {
            LoadingFailedEvent.Invoke();
        }

        //Debug.Log("NewMapStringArrivedEvent(mapString): \n" + mapString.Substring(0,1000) + "\n\n" );
        Debug.Log("New Map-string arrived. Sending the event for it.");

        Debug.Log("OnMapStringArrived.");
        LoadingOutput.Log("Map-String has arrived.");

        float latestTime = Time.time;

        MapRequester.ShowLoadingProgress(0.91f, "Extracting data... ");
        mapData.ExtractData(mapString);

        yield return null;
        float timeForBasicData = Time.time - latestTime;
        latestTime = Time.time;

        MapRequester.ShowLoadingProgress(0.92f, "Calculating Hex-Fields... ");
        List<HexCenterPoint> newHexCenters = hexCreator.CreateNecessaryHexCenters(mapData.LatestOsmBounds.AreaCenter, hexGenerations);
        Debug.Log("newHexCenters produced " + newHexCenters.Count + " new Hexes...");
        yield return null;
        float timeForHexCreation = Time.time - latestTime;
        latestTime = Time.time;

        MapRequester.ShowLoadingProgress(0.94f, "Creating Geometry... ");
        if (CreatingGeometryEvent != null)
            CreatingGeometryEvent(mapData.LatestOsmBounds, mapData.Nodes, mapData.NewWays, newHexCenters);

        yield return null;
        float timeForGeometryCreation = Time.time - latestTime;
        latestTime = Time.time;

        if (ContentCreationFinishedEvent != null)
            ContentCreationFinishedEvent();

        yield return null;
        float timeForFinish = Time.time - latestTime;

        Debug.Log("Times during Loading: \n" +
            "timeForBasicData: " + timeForBasicData + "s\n" +
            "timeForHexCreation: " + timeForHexCreation + "s\n" +
            "timeForGeometryCreation: " + timeForGeometryCreation + "s\n" +
            "timeForFinish: " + timeForFinish + "\n" +
            "");

        MapRequester.ShowLoadingProgress(1, "Done.");
    }

    private bool IsRequestNecessary(PolarCoordinates center)
    {
        if (mapStringProvider.IsWaitingForMap)
        {
            Debug.Log("I would request more map-data, but I am already waiting for a map to arrive.");
            return false;
        }
        if (DistanceToNearestAreaCenter(center) < 15)//Todo: calculate the save distance from radius (which is in degree) or better even: check for each center it's meter-"radius".
        {
            return false;
        }
        return true;
    }
    private float DistanceToNearestAreaCenter(PolarCoordinates position)
    {
        float minDistance = float.MaxValue;

        foreach (PolarCoordinates requestedCenter in knownAreaCenters)
        {
            minDistance = Mathf.Min(minDistance, PolarCoordinates.DistanceMeters(position, requestedCenter));
        }
        return minDistance;
    }
    private bool IsAnswerLegal(string mapString)
    {

        if (mapString == "")
        {
            Debug.LogError("The map String is empty! Do you have internet-connection?");
            LoadingOutput.Log("The map String is empty! Do you have internet-connection?");
            return false;
                
        }
        return true;
    }

}