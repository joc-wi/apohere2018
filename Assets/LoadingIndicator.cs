﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingIndicator : MonoBehaviour {
    [SerializeField] private GameObject groupToHide;
    [SerializeField] private RectTransform LoadingBarFilling;
    [SerializeField] private Text LoadingText;
	// Use this for initialization
	void Start () {
        MapRequester.ShowProgressEvent += LoadingProgressHandler;
        groupToHide.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    private void LoadingProgressHandler(float progress, string message)
    {
        groupToHide.SetActive(true);
        LoadingText.text = message;
        LoadingBarFilling.localScale = new Vector3(progress, 1, 1);

        if(progress == 1)
        {
            groupToHide.SetActive(false);
        }
    }
}
