// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "New AmplifyShader"
{
	Properties
	{
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) fixed3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float3 worldPos;
			float3 viewDir;
			INTERNAL_DATA
			float3 worldNormal;
		};

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float3 ase_worldPos = i.worldPos;
			float3 temp_output_33_0_g4 = i.worldNormal;
			float3 normalizeResult17_g4 = normalize( cross( cross( i.viewDir , temp_output_33_0_g4 ) , temp_output_33_0_g4 ) );
			float ifLocalVar3_g1 = 0;
			if( ( sin( ( ase_worldPos * float3( 10,40,20 ) ) ) + float3( 1,1,1 ) ).x <= 1.95 )
				ifLocalVar3_g1 = 1.0;
			else
				ifLocalVar3_g1 = 0.0;
			float ifLocalVar2_g1 = 0;
			if( ( sin( ( ase_worldPos * float3( 10,40,20 ) ) ) + float3( 1,1,1 ) ).y <= 1.95 )
				ifLocalVar2_g1 = 1.0;
			else
				ifLocalVar2_g1 = 0.0;
			float ifLocalVar1_g1 = 0;
			if( ( sin( ( ase_worldPos * float3( 10,40,20 ) ) ) + float3( 1,1,1 ) ).z <= 1.95 )
				ifLocalVar1_g1 = 1.0;
			else
				ifLocalVar1_g1 = 0.0;
			float temp_output_6_0_g1 = ( ifLocalVar3_g1 * ifLocalVar2_g1 * ifLocalVar1_g1 );
			float temp_output_3_0_g4 = ( temp_output_6_0_g1 * 0.1 );
			float dotResult18_g4 = dot( i.viewDir , temp_output_33_0_g4 );
			float temp_output_31_0_g4 = sqrt( ( ( 1.0 / dotResult18_g4 ) + -1.0 ) );
			float3 temp_output_19_0_g4 = ( normalizeResult17_g4 * temp_output_3_0_g4 * temp_output_31_0_g4 );
			float3 temp_output_18_4 = ( ase_worldPos + temp_output_19_0_g4 );
			float3 temp_output_33_0_g6 = temp_output_18_4;
			float3 normalizeResult17_g6 = normalize( cross( cross( i.viewDir , temp_output_33_0_g6 ) , temp_output_33_0_g6 ) );
			float ifLocalVar3_g5 = 0;
			if( ( sin( ( temp_output_18_4 * float3( 10,40,20 ) ) ) + float3( 1,1,1 ) ).x <= 1.95 )
				ifLocalVar3_g5 = 1.0;
			else
				ifLocalVar3_g5 = 0.0;
			float ifLocalVar2_g5 = 0;
			if( ( sin( ( temp_output_18_4 * float3( 10,40,20 ) ) ) + float3( 1,1,1 ) ).y <= 1.95 )
				ifLocalVar2_g5 = 1.0;
			else
				ifLocalVar2_g5 = 0.0;
			float ifLocalVar1_g5 = 0;
			if( ( sin( ( temp_output_18_4 * float3( 10,40,20 ) ) ) + float3( 1,1,1 ) ).z <= 1.95 )
				ifLocalVar1_g5 = 1.0;
			else
				ifLocalVar1_g5 = 0.0;
			float temp_output_6_0_g5 = ( ifLocalVar3_g5 * ifLocalVar2_g5 * ifLocalVar1_g5 );
			float temp_output_3_0_g6 = ( temp_output_6_0_g5 * 0.1 );
			float dotResult18_g6 = dot( i.viewDir , temp_output_33_0_g6 );
			float temp_output_31_0_g6 = sqrt( ( ( 1.0 / dotResult18_g6 ) + -1.0 ) );
			float3 temp_output_19_0_g6 = ( normalizeResult17_g6 * temp_output_3_0_g6 * temp_output_31_0_g6 );
			float ifLocalVar3_g3 = 0;
			if( ( sin( ( ( ( temp_output_18_4 + ( ase_worldPos + temp_output_19_0_g6 ) ) * float3( 0.5,0.5,0.5 ) ) * float3( 10,40,20 ) ) ) + float3( 1,1,1 ) ).x <= 1.95 )
				ifLocalVar3_g3 = 1.0;
			else
				ifLocalVar3_g3 = 0.0;
			float ifLocalVar2_g3 = 0;
			if( ( sin( ( ( ( temp_output_18_4 + ( ase_worldPos + temp_output_19_0_g6 ) ) * float3( 0.5,0.5,0.5 ) ) * float3( 10,40,20 ) ) ) + float3( 1,1,1 ) ).y <= 1.95 )
				ifLocalVar2_g3 = 1.0;
			else
				ifLocalVar2_g3 = 0.0;
			float ifLocalVar1_g3 = 0;
			if( ( sin( ( ( ( temp_output_18_4 + ( ase_worldPos + temp_output_19_0_g6 ) ) * float3( 0.5,0.5,0.5 ) ) * float3( 10,40,20 ) ) ) + float3( 1,1,1 ) ).z <= 1.95 )
				ifLocalVar1_g3 = 1.0;
			else
				ifLocalVar1_g3 = 0.0;
			float temp_output_6_0_g3 = ( ifLocalVar3_g3 * ifLocalVar2_g3 * ifLocalVar1_g3 );
			float4 temp_output_20_0_g3 = float4(0.5147059,0.4798875,0.4049524,0);
			float4 ifLocalVar16_g3 = 0;
			if( temp_output_6_0_g3 <= 0.0 )
				ifLocalVar16_g3 = temp_output_20_0_g3;
			else
				ifLocalVar16_g3 = float4(0.5441177,0.1400303,0.1400303,0);
			o.Emission = ifLocalVar16_g3.rgb;
			o.Alpha = 1;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			# include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float4 tSpace0 : TEXCOORD1;
				float4 tSpace1 : TEXCOORD2;
				float4 tSpace2 : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				fixed3 worldNormal = UnityObjectToWorldNormal( v.normal );
				fixed3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				fixed tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				fixed3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			fixed4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				fixed3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.viewDir = worldViewDir;
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13801
1927;29;1906;1004;2385.05;1298.351;1.700933;True;True
Node;AmplifyShaderEditor.WorldPosInputsNode;9;-1630.706,-1026.674;Float;False;0;4;FLOAT3;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.FunctionNode;8;-1419.346,-654.4931;Float;False;BrickFunction;-1;;1;9444496ad7471714f8245a1427c24e10;4;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT3;0,0,0;False;3;COLOR;0,0,0,0;False;2;FLOAT;COLOR
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;34;-1081.131,-845.8934;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.1;False;1;FLOAT
Node;AmplifyShaderEditor.FunctionNode;18;-948.9844,-695.5836;Float;False;WorldSpaceParallax;-1;;4;bdadea4f906f4dd419eebd0a814f9fc1;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0.0;False;5;FLOAT;FLOAT3;FLOAT;FLOAT3;FLOAT3
Node;AmplifyShaderEditor.FunctionNode;21;-1463.662,-434.8815;Float;False;BrickFunction;-1;;5;9444496ad7471714f8245a1427c24e10;4;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT3;0,0,0;False;3;COLOR;0,0,0,0;False;2;FLOAT;COLOR
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;35;-1063.611,-232.693;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.1;False;1;FLOAT
Node;AmplifyShaderEditor.FunctionNode;22;-993.3008,-475.972;Float;False;WorldSpaceParallax;-1;;6;bdadea4f906f4dd419eebd0a814f9fc1;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0.0;False;5;FLOAT;FLOAT3;FLOAT;FLOAT3;FLOAT3
Node;AmplifyShaderEditor.SimpleAddOpNode;25;-351.5374,-700.3392;Float;False;2;2;0;FLOAT3;0.0;False;1;FLOAT3;0.0,0,0;False;1;FLOAT3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;26;-164.5612,-705.6061;Float;False;2;2;0;FLOAT3;0.0;False;1;FLOAT3;0.5,0.5,0.5;False;1;FLOAT3
Node;AmplifyShaderEditor.ColorNode;15;-909.6185,736.9732;Float;False;Constant;_Color0;Color 0;0;0;0.5441177,0.1400303,0.1400303,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;16;-1291.471,673.7697;Float;False;Constant;_Color1;Color 1;0;0;0.5147059,0.4798875,0.4049524,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.FunctionNode;14;-41.25785,613.2;Float;False;BrickFunction;-1;;3;9444496ad7471714f8245a1427c24e10;4;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT3;0,0,0;False;3;COLOR;0,0,0,0;False;2;FLOAT;COLOR
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;349.3566,190.1393;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;New AmplifyShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;8;0;9;0
WireConnection;34;0;8;0
WireConnection;18;1;9;0
WireConnection;18;2;34;0
WireConnection;21;0;18;4
WireConnection;35;0;21;0
WireConnection;22;0;18;4
WireConnection;22;2;35;0
WireConnection;25;0;18;4
WireConnection;25;1;22;4
WireConnection;26;0;25;0
WireConnection;14;0;26;0
WireConnection;14;1;16;0
WireConnection;14;3;15;0
WireConnection;0;2;14;1
ASEEND*/
//CHKSM=72E4BF5784C2A3B1AB0F6EF5C942294560F5501B