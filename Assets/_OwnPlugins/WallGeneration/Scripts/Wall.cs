﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace WallGeneration
{
    public class Wall : MonoBehaviour
    {

        [SerializeField]
        public Vector3 StartPoint;
        [SerializeField]
        public Vector3 EndPoint;
        public int NumberOfStories;
        
        [SerializeField]
        private Vector3 wallDirection;
        [SerializeField]
        private Material directedMaterial;

        public List<WallPiece> availableWallPiecePrefab;
        private int numberOfWallSegments;
        private float scalingOfPieces;
        struct WallSegment
        {
            public bool isAboveBasement;
            public bool hasBeenFinished;
            public int numberOfStories;
        }
        private WallSegment[] wallSegments;


        // Use this for initialization
        void Start()
        {
            //updateValues();
            //GenerateWall();	
        }


        public void SetWall(Vector3 startPoint, Vector3 endPoint, int stories, Material directedMaterial = null)
        {
            StartPoint = startPoint;
            EndPoint = endPoint;
            this.NumberOfStories = stories;
            if (directedMaterial != null)
            {
                //directedMaterial.enableInstancing = false;
                this.directedMaterial = Material.Instantiate<Material>(directedMaterial);
                
            }
            else
            {
                // it is important to have an _instance_ of the material, so we can change properties.
                this.directedMaterial = Material.Instantiate<Material>(this.directedMaterial);
            }
            
            CalculateSecondaryValues();
            GenerateWall();
        }
        private void CalculateSecondaryValues()
        {
            numberOfWallSegments = Mathf.Max(1, (int)(Vector3.Distance(StartPoint, EndPoint) / WallSettings.wallSegmentLength));
            scalingOfPieces = Vector3.Distance(StartPoint, EndPoint) / (WallSettings.wallSegmentLength * numberOfWallSegments);
            wallDirection = (EndPoint - StartPoint).normalized;
            directedMaterial.SetVector("_WallDirectionNormalized", new Vector4(wallDirection.x, wallDirection.y, wallDirection.z, 0));
        }

        private void GenerateWall()
        {
            wallSegments = new WallSegment[numberOfWallSegments];
            //Debug.Log("GenerateWall: numberOfWallSegments=" + numberOfWallSegments);
            //GenerateWallBasement();
            for (int story = 0; story <= this.NumberOfStories; story++)
            {
                //Debug.Log("story=" + story + " NumberOfStories=" + NumberOfStories);
                GenerateWallStory(story);
            }
        }

        private void GenerateWallStory(int story)
        {
            for (int position = 0; position < numberOfWallSegments; position++)
            {
                //Debug.Log("inside GenerateWallStory, potition=" + position);
                if (wallSegments[position].numberOfStories != story)
                {
                    continue;
                }
                if (wallSegments[position].hasBeenFinished)
                {
                    continue;
                }
                List<WallPiece> fittingWallPieces = availableWallPiecePrefab.Where(x => CanWallPieceFit(x, position, story)).ToList<WallPiece>(); //todo: a bit more selection here!
                int randomIndex = Random.Range(0, fittingWallPieces.Count);
                //Debug.Log("randomIndex: " + randomIndex + ", fittingWallPieces.Count" + fittingWallPieces.Count);
                Debug.Assert(fittingWallPieces.Count > 0, "No wallPiece fits the criteria!");
                WallPiece pieceA = fittingWallPieces[randomIndex];

                PlaceWallPieceInWall(GameObject.Instantiate(pieceA), position, story);
                AddPieceOfWallInWallSegments(pieceA, position);
            }

        }
        private void PlaceWallPieceInWall(WallPiece piece, int position, int story)
        {
            piece.SetMaterial(directedMaterial, wallDirection);
            piece.transform.SetParent(this.transform);
            piece.transform.localPosition = StartPoint + position * (EndPoint - StartPoint) / numberOfWallSegments + Vector3.up * WallSettings.storyHeight * story;
            Vector3 worldspaceEndpoint = new Vector3(EndPoint.x, piece.transform.position.y, EndPoint.z);
            piece.transform.LookAt(this.transform.parent.TransformPoint(worldspaceEndpoint), Vector3.up);

            piece.transform.localScale = new Vector3(1, 1, scalingOfPieces);
            
        }
        private void AddPieceOfWallInWallSegments(WallPiece piece, int position)
        {
            for (int i = position; i < position + piece.lengthInSegments; i++)
            {
                wallSegments[i].numberOfStories += piece.heigthInStories;
                wallSegments[i].isAboveBasement = true;
                wallSegments[i].hasBeenFinished = piece.isCap;
            }
        }
        private bool CanWallPieceFit(WallPiece piece, int position, int story)
        {
            
            // Basementpieces:
            if ((story == 0) != (piece.isBasement))
            {
                //Debug.Log("piece does not fit because it is not a basement.");
                return false;
            }

            // On the upperMostLevel, there can be only cap-pieces:
            if (story == NumberOfStories-1 && piece.isCap)
            {
                //Debug.Log("piece does not fit because it is not a basement.");
                return false;
            }
            if (position + piece.lengthInSegments > numberOfWallSegments)
                return false;

            for (int i = position; i < position + piece.lengthInSegments; i++)
            {
                if (wallSegments[i].hasBeenFinished)
                {
                    //Debug.Log("piece does not fit because this segment has been finished.");
                    return false;
                }
                if (wallSegments[i].numberOfStories != story)
                {
                    //Debug.Log("piece does not fit because this segment is to high: story = " + story + ", wallSegments[i].numberOfStories=" + wallSegments[i].numberOfStories);
                    return false;
                }
            }
            return true;
        }
    }
}
