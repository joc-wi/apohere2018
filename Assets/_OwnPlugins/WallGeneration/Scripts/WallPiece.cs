﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace WallGeneration
{ 
    public class WallPiece : MonoBehaviour
    {
        public int heigthInStories;
        //public float length;
        public int lengthInSegments;
        public bool isCap;
        public bool isBasement;

        /// <summary>
        /// Easy way to access the WallPiece's Material.
        /// Careful: this is not implemented in a fast way.
        /// Does override _all_ materials of children at the moment.
        /// </summary>
        public void SetMaterial(Material directedMaterial, Vector3 wallDirection)
        {
            
            Renderer[] renderers = GetComponentsInChildren<Renderer>();
            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].material = directedMaterial;
                //renderers[i].material.SetVector("_WallDirectionNormalized", new Vector4(wallDirection.x, wallDirection.y, wallDirection.z, 0));
            }

        }
    }
}