﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlantGeneration
{
    public class LeafBunch : MonoBehaviour
    {

        public void Initialize(Color baseColor, Vector3 basePosition)
        {
            transform.localPosition = basePosition + new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), Random.Range(-1, 1));

            transform.up = - Camera.main.transform.forward;
            transform.Rotate(new Vector3(0, 1, 0), Random.Range(0, 360));

            //SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
            //Debug.Log("Setting leaf color to " + baseColor);
            //spriteRenderer.material.SetColor("_LeafColor", RandomColorVariation(baseColor));
            //spriteRenderer.material.SetColor("_main", RandomColorVariation(baseColor));

            Renderer renderer = GetComponent<Renderer>();
            //Debug.Log("Setting leaf color to " + baseColor);
            //spriteRenderer.material.SetColor("_LeafColor", RandomColorVariation(baseColor));
            renderer.material.color = RandomTools.RandomColorVariation(baseColor, 0.15f);

        }
        
        
    }

    
}
