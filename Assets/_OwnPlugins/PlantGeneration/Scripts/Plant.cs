﻿using Deserialisation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlantGeneration
{ 
    public class Plant : MonoBehaviour
    {
        [SerializeField]
        internal Branch branchPrefab;
        [SerializeField]
        internal LeafBunch leafsPrefab;
        [SerializeField]
        internal Color leafBaseColor;
        public int Id { get; protected set; }


        

        public virtual void Initialize(PolarCoordinates coordinates, OsmBounds currentBounds, float age)
        {
            Vector3 position = coordinates.UnitySpace;
            int randomSeed = (int)(coordinates.lat * coordinates.lon * 10);
            this.Initialize(position, age, randomSeed);
        }

        public virtual void Initialize(Vector3 position, float age, int randomSeed)
        {
            leafBaseColor = RandomTools.RandomColorVariation(leafBaseColor, 0.2f);
            transform.localPosition = position;
            Vector3 startDirection = new Vector3(0, 1, 0);
            Branch branch = Instantiate(branchPrefab, this.transform);
            branch.transform.localPosition = Vector3.zero;
            branch.StartNewBranch(startDirection, age, randomSeed, this);
        }
    }
}