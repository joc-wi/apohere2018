﻿using PlantGeneration;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript_InitializePlant : MonoBehaviour {


    public Plant plantPrefab;
	// Use this for initialization
	void Start () {
        Plant plant = Instantiate(plantPrefab, this.transform);


        plant.Initialize(Vector3.zero, 6, 1234);
	}
	
	
}
