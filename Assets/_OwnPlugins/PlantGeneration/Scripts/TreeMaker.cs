﻿using System.Collections;
using System.Collections.Generic;
using Deserialisation;
using UnityEngine;
using WorldToHexagons;
using PlantGeneration;

namespace GeometyCreation
{
    class TreeMaker : GeometryMakerBehavior
    {   [SerializeField]
        private Plant plantPrefab;

        private Dictionary<HexCenterPoint, List<Plant>> plants = new Dictionary<HexCenterPoint, List<Plant>>();

        public override void CreateGeometry(OsmBounds currentBounds, Dictionary<ulong, OsmNode> allNodes, Dictionary<ulong, OsmWay> additionalWays, List<HexCenterPoint> hexCenters)
        {
            Debug.Log("TreeGeneration has been triggered!\n" +
                "Number of hexCenters = " + hexCenters.Count +
                "");


            CreateParentGroup("Trees");
            
            foreach(HexCenterPoint hexCenter in hexCenters)
            {
                
                EnsurePlants(hexCenter, currentBounds);


            }


        }

        protected void EnsurePlants(HexCenterPoint hexCenter, OsmBounds currentBounds)
        {
            if (plants.ContainsKey(hexCenter))
            {
                return;
            }
            if (WaterArea.IsInWater(hexCenter.PolarCoordinates))
            {
                return;
            }
            List<Plant> plantsOnHex = new List<Plant>();
            plants.Add(hexCenter, plantsOnHex);

            PolarCoordinates coordinates = hexCenter.PolarCoordinates;
            float plantiness = Random.Range(2, 10); //Todo: plantiness should become part of hex.
            // Center-plant
            Plant centerPlant = Instantiate(plantPrefab);
            Vector3 centerPosition = hexCenter.PolarCoordinates.UnitySpace;
            float age = plantiness;
            centerPlant.Initialize(centerPosition, age, 123);
            plantsOnHex.Add(centerPlant);
            centerPlant.transform.SetParent(parentGroup);

            // Other plants:
            float maxDistance = 6 + plantiness;
            for (int i =1; i<plantiness; i++)
            {
                Plant plant = Instantiate(plantPrefab);
                
                Vector3 position = centerPosition + new Vector3(Random.Range(-maxDistance, +maxDistance),0,Random.Range(-maxDistance, +maxDistance));
                age = Random.Range(2, plantiness);
                plant.Initialize(position, age, 123);
                plantsOnHex.Add(plant);

                plant.transform.SetParent(parentGroup);
            }
            
        }
    }
}