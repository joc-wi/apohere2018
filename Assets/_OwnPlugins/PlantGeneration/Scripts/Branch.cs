﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PlantGeneration
{
    public class Branch : MonoBehaviour
    {
        [SerializeField]
        LineRenderer lineRenderer;

        public Vector3 direction;

        public void StartNewBranch(Vector3 startDirection, float ageOfSegment, int randomSeed, Plant plant)
        {
            lineRenderer.widthMultiplier = ageOfSegment * 0.1f +0.3f;
            CreateSegment(startDirection, ageOfSegment, randomSeed, plant);
        }
        protected void CreateSegment(Vector3 startDirection, float ageOfSegment, int randomSeed, Plant plant)
        {
            //Debug.Log("Creating BranchSegment...");

            int formerPositionCount = lineRenderer.positionCount;
            Vector3[] newPositions = new Vector3[formerPositionCount+1];
            lineRenderer.GetPositions(newPositions);
            float length = Mathf.Sqrt(ageOfSegment);
            direction = startDirection.normalized * length;
            Vector3 endKnotPosition = newPositions[formerPositionCount - 1] + direction;
            newPositions[formerPositionCount] = endKnotPosition;

            lineRenderer.positionCount = (formerPositionCount + 1);
            lineRenderer.SetPositions(newPositions);

            float primaryRotationY = Random.Range(0, 360);
            float secondaryRotationY = primaryRotationY + 180 + Random.Range(-35, 35);
            if (ageOfSegment > 1)
            {
                Vector3 nextDirection = Quaternion.Euler(Random.Range(0,  45), primaryRotationY, 0 ) * startDirection;
                //BranchSegment branchSegment = Instantiate(branchSegmentPrefab);
                this.CreateSegment(nextDirection, ageOfSegment - 1, randomSeed, plant);
            }
            if (ageOfSegment > 1.5)
            {
                //LineRenderer additionalLineRenderer = lineRenderer.gameObject.AddComponent<LineRenderer>();

                Vector3 nextDirection = Quaternion.Euler(Random.Range(0, 65), secondaryRotationY, 0) * startDirection;
                Branch branchSegment = Instantiate(plant.branchPrefab, this.transform);
                branchSegment.transform.localPosition = endKnotPosition;
                branchSegment.StartNewBranch(nextDirection, ageOfSegment - 1.5f, randomSeed, plant);
            }
            if (ageOfSegment < 1.5)
            {
                LeafBunch leafes = Instantiate<LeafBunch>(plant.leafsPrefab, this.transform);
                leafes.Initialize(plant.leafBaseColor, endKnotPosition);
            }
        }
        
    }
}