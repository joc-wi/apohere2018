﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FastLookAtCamera : MonoBehaviour {

	// Use this for initialization
	void Start () {
        transform.forward = Camera.main.transform.forward;
    }
	
	// Update is called once per frame
	void LateUpdate () {
        //transform.forward = Camera.main.transform.forward;
    }
}
