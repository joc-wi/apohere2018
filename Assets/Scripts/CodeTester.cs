﻿using ProjectionStrategies;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeTester : MonoBehaviour {
    static private IProjectionStrategy Projector;


    // Use this for initialization
    void Start ()
    {
        TestProjector();

        TestAngleCalculation();


        //Debug.Log(
        //    "PolarCoordinates.UnitSphereToPolar(new Vector3(0, 0.44721365f, 0.89442718f) (Sahara) = " + 
        //    PolarCoordinates.UnitSphereToPolar(new Vector3(0, 0.44721365f, 0.89442718f))
        //    );
        //Debug.Log(
        //    "PolarCoordinates.UnitSphereToPolar(new Vector3(1, 0, 0) (somethig on the equator) = " +
        //    PolarCoordinates.UnitSphereToPolar(new Vector3(1, 0, 0))
        //    );


    }
	
	private void TestProjector()
    {
        Projector = StrategyServiceProvider.Instance.Projector;
        //Debug.Log("Projector.LatToMapX(Projector.XToLat(7000)) = " + Projector.LatToMapX(Projector.XToLat(7000)));
        //Debug.Log("Projector.XToLat(Projector.LatToMapX(7)) = " + Projector.XToLat(Projector.LatToMapX(7)));
        //Debug.Log("Projector.LonToMapY(Projector.YToLon(7000)) = " + Projector.LonToMapY(Projector.YToLon(7000)));
        //Debug.Log("Projector.YToLon(Projector.LatToMapX(7)) = " + Projector.YToLon(Projector.LonToMapY(7)));


        Debug.Assert(
            NumbersAreSimilar(
                Projector.LatToMapX(Projector.XToLat(7000)),
                7000)
            );
        Debug.Assert(NumbersAreSimilar(Projector.XToLat(Projector.LatToMapX(7)), 7));
        Debug.Assert(NumbersAreSimilar(Projector.LonToMapY(Projector.YToLon(7000)), 7000));
        Debug.Assert(NumbersAreSimilar(Projector.YToLon(Projector.LonToMapY(7)), 7));
    }

    private void TestAngleCalculation()
    {

        // Again: North-Pole and some point on the Equator:
        double angle;
        angle = PolarCoordinates.DistanceAngleEuler(new PolarCoordinates(90, 0), new PolarCoordinates(0, 0));
        Debug.Assert(NumbersAreSimilar(angle , 90), "Angle should be 90 but is " + angle);

        // SouthPole:
        angle = PolarCoordinates.DistanceAngleEuler(new PolarCoordinates(-90, 123), new PolarCoordinates(0, -77));
        Debug.Assert(NumbersAreSimilar(angle , 90), "Angle should be 90 but is " + angle);

        // an angle with similar lon across aequator:
        angle = PolarCoordinates.DistanceAngleEuler(new PolarCoordinates(-60, 77), new PolarCoordinates(30, 77));
        Debug.Assert(NumbersAreSimilar(angle , 90), "Angle should be 90 but is " + angle);

        // an angle with similar lat along the aequator:
        angle = PolarCoordinates.DistanceAngleEuler(new PolarCoordinates(0, -30), new PolarCoordinates(0, 30));
        Debug.Assert(NumbersAreSimilar(angle, 60), "Angle should be 60 but is " + angle);

        // an angle with similar lat along the aequator, crossing the international date-line:
        angle = PolarCoordinates.DistanceAngleEuler(new PolarCoordinates(0, -170), new PolarCoordinates(0, 170));
        Debug.Assert(NumbersAreSimilar(angle,20), "Angle should be 20 but is " + angle);

        // an angle across north-pole:
        angle = PolarCoordinates.DistanceAngleEuler(new PolarCoordinates(80, -30), new PolarCoordinates(70, 150));
        Debug.Assert(NumbersAreSimilar(angle, 30), "Angle should be 30 but is " + angle);
    }


    public static bool NumbersAreSimilar(double a, double b)
    {
        double biggestAbs = Math.Max(Math.Abs(a), Math.Abs(b));
        double delta = Math.Abs(a - b);
        return (delta < biggestAbs * 0.001) ;
    }
}
