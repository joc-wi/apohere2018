﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zoom : MonoBehaviour {
    private float zoom;
    [SerializeField] private float sensitivity = 10;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		zoom += Input.GetAxis("Mouse ScrollWheel") * sensitivity;
        zoom = Mathf.Clamp(zoom, -200f, 10f);
        transform.localPosition = Vector3.forward * zoom;
	}
    
}
