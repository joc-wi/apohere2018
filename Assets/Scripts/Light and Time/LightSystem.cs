﻿using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSystem : MonoBehaviour {

    //string debugString;

    [SerializeField]
    private DayPhaseData[] dayPhaseDatas;

    [SerializeField]
    private float dayAcceleration=96f;

    private const float degreesPerHour = 15;
    [SerializeField]
    private float startTimeInHours = 8;
    [SerializeField]
    Light Sun;
    
    [SerializeField]
    Light Moon;
    

    private float dayTimeInHours;


    int latestDayPhaseId;
    DayPhaseData previousPhase;
    DayPhaseData nextPhase;
    float timeSpan;

    private void Start()
    {
        latestDayPhaseId=0;
        UpdatePhases(latestDayPhaseId);
    }
    void Update () {
        dayTimeInHours = ((Time.time * dayAcceleration/3600f) + startTimeInHours) % 24;
        float rotationInEuler = dayTimeInHours * degreesPerHour;
        this.transform.rotation = Quaternion.Euler(0,0,rotationInEuler);

        if (dayTimeInHours < dayPhaseDatas[1].hour && latestDayPhaseId >1)
        {
            latestDayPhaseId = 0;
            UpdatePhases(latestDayPhaseId);
        }
        while(IsNextDayPhaseGoneBy())
        {
            SwitchToNextDayPhase();
            UpdatePhases(latestDayPhaseId);
        }
        
        //debugString = ("Time = "+dayTimeInHours + ", previousPhase=" + previousPhase.name+", Moon-Color = "+Moon.color+"");
        

        Debug.Assert(previousPhase != null);
        Debug.Assert(nextPhase != null);
        UpdateSunColor();
        UpdateMoonColor();
    }
    private bool IsNextDayPhaseGoneBy()
    {
        DayPhaseData nextDayPhase = dayPhaseDatas[(latestDayPhaseId + 1) % dayPhaseDatas.Length];
       // Debug.Log("checking " + nextDayPhase.name);
        return (nextDayPhase.hour < dayTimeInHours);
    }
    private void SwitchToNextDayPhase()
    {
        latestDayPhaseId = (latestDayPhaseId + 1) % dayPhaseDatas.Length;
        
    }
    private void UpdatePhases(int latestDayPhaseId)
    {
        previousPhase = dayPhaseDatas[latestDayPhaseId];
        nextPhase = dayPhaseDatas[(latestDayPhaseId + 1) % dayPhaseDatas.Length];
        //Debug.Log("Switched to next Day Phase: " + previousPhase.name);

        timeSpan = nextPhase.hour - previousPhase.hour;
        Moon.enabled = previousPhase.moonEnabledFromHereOn;
        Sun.enabled = previousPhase.sunEnabledFromHereOn;
    }
    private void UpdateSunColor()
    {
        if (Sun.enabled)
        {
            Sun.color = Color.Lerp(
                previousPhase.sunColor,
                nextPhase.sunColor,
                (dayTimeInHours - previousPhase.hour) / timeSpan
                );
        }
    }
    private void UpdateMoonColor()
    {
        if (!Moon.enabled)
            return;

        //Debug.Log("SettingMoonColor. " + previousPhase.name + "-color: " + previousPhase.moonColor + "\n" + nextPhase.name + "-color: " + nextPhase.moonColor);
            
        Moon.color = Color.Lerp(
            previousPhase.moonColor,
            nextPhase.moonColor, 
            (dayTimeInHours-previousPhase.hour) / timeSpan
            );
            
        
    }
}
