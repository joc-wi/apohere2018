﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Jochen/DayPhaseData",fileName ="DayPhaseData")]
[Serializable]
public class DayPhaseData : ScriptableObject {
    [SerializeField]
    internal new string name;
    [SerializeField]
    internal float hour;
    [SerializeField]
    internal bool sunEnabledFromHereOn;
    [SerializeField]
    internal Color sunColor;
    [SerializeField]
    internal bool moonEnabledFromHereOn;
    [SerializeField]
    internal Color moonColor;
}
