﻿using Deserialisation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisappearOnLoad : MonoBehaviour {
    
    protected virtual void Awake()
    {
        //Debug.Log("GeometryMakerBehavior.Awake() just got called. Should subscribe to dataProvider.CreateGeometryEvent");

        RequestRailroad.ContentCreationFinishedEvent += disappear;
        this.gameObject.SetActive(true);
    }
    public  void disappear()
    {
        this.gameObject.SetActive(false);
    }
}
