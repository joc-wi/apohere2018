﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StreetNameIndicator : MonoBehaviour {

    [SerializeField]
    private Text textField;
	public string StreetName
    {
        set
        {
            textField.text = value;
        }
    }
}
