﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WorldToHexagons;

public class TestHexMaker : MonoBehaviour {
    HexCreator hexCreator = new HexCreator();
	// Use this for initialization
	void Start () {


    }
    public void TestPlusPlus()
    {
        TestCoordinates(new PolarCoordinates(40, 7), 18);
    }
    public void TestPlusMinus()
    {
        TestCoordinates(new PolarCoordinates(40, -7), 18);
    }
    public void TestMinusPlus()
    {
        TestCoordinates(new PolarCoordinates(-40, 7), 18);
    }
    public void TestMinusMinus()
    {
        TestCoordinates(new PolarCoordinates(-40, -7), 18);
    }
    private void TestCoordinates(PolarCoordinates areaCenter, uint generations)
    {
        Deserialisation.CenterOfUnitySpace.SetCenter(areaCenter.lat - 0.001, areaCenter.lon - 0.001, areaCenter.lat + 0.001, areaCenter.lon + 0.001);
        List<HexCenterPoint> list= hexCreator.CreateNecessaryHexCenters(areaCenter, generations);
        Debug.Assert(list != null);
        if (IsBetweenCenters(areaCenter, list))
        {
            Debug.Log("Good! The position is between the final centers!");
        }
        else
        {
            Debug.LogError("Not good! The Position is _not_ between the final centers!");
        }

    }

    private bool IsBetweenCenters(PolarCoordinates position, List<HexCenterPoint> centers)
    {Debug.Assert(centers != null);
        double maxLat = double.MinValue;
        double maxLon = double.MinValue;
        double minLat = double.MaxValue;
        double minLon = double.MaxValue;
        
        foreach (HexCenterPoint center in centers)
        {
            maxLat = Math.Max(maxLat, center.PolarCoordinates.lat);
            maxLon = Math.Max(maxLon, center.PolarCoordinates.lon);
            minLat = Math.Min(minLat, center.PolarCoordinates.lat);
            minLon = Math.Min(minLon, center.PolarCoordinates.lon);
        }

        if (position.lat > maxLat)
            return false;
        if(position.lon > maxLon)
        {
            return false;
        }
        if(position.lat< minLat){
            return false;
        }
        if(position.lon < minLon)
        {
            return false;
        }
        return true;
    }
}
