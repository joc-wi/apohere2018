﻿using UnityEngine;

namespace ProjectionStrategies
{
    public interface IProjectionStrategy
    {
        
        /// <summary>
        /// polar to map-space
        /// </summary>
        double LatToMapX(double lat);

        /// <summary>
        /// polar to map-space
        /// </summary>
        double LonToMapY(double lon);

        Vector2 ToPolar(double x, double y);

        double XToLat(double x);
        double YToLon(double y);
        double ZToLon(double z);

        Vector3 PolarToX0Z(double lat, double lon);
    }
}