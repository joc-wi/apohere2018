﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// 
/// </summary>
namespace ProjectionStrategies
{
    /// <summary>
    /// A Mercator-projection projects the Earth onto a cylinder.
    /// It is neither the fastest nor the most correct projection.
    /// 
    /// Code mainly from here: http://wiki.openstreetmap.org/wiki/Mercator
    /// C# Implementation by Florian Müller, based on the C code published on the same site.
    /// </summary>
    public class EllipticalMercator : IProjectionStrategy
    {
        private static readonly double EarthRadius_MAJOR = 6378137.0;
        private static readonly double EarthRadius_MINOR = 6356752.3142;
        private static readonly double RATIO = EarthRadius_MINOR / EarthRadius_MAJOR;
        private static readonly double ECCENT = Math.Sqrt(1.0 - (RATIO * RATIO));
        private static readonly double COM = 0.5 * ECCENT;

        private static readonly double DEG2RAD = Math.PI / 180.0;
        private static readonly double RAD2Deg = 180.0 / Math.PI;
        private static readonly double PI_2 = Math.PI / 2.0;

        public double[] ToPixel(double lon, double lat)
        {
            Debug.LogWarning("Any exiting use of this function is likely buggy at the moment due to a switch of lat and long and x and y");
            return new double[] { LatToMapX(lat), LonToMapY(lon)  };
        }

        /// <summary>
        /// EPSG:4326 specifically states that the coordinate order should be latitude, longitude.
        /// </summary>
        /// <param name="x">latitude</param>
        /// <param name="y">longitude</param>
        /// <returns></returns>
        public Vector2 ToPolar(double x, double y)
        {
            Vector2 result=new Vector2 ( (float)XToLat(x),(float)YToLon(y) );
            //Debug.Log("ToGeoCoord(" + x + ", " + y + ") => " + result.ToString("G7"));
            return result;
        }

        public double LonToMapY(double lon)
        {
            return EarthRadius_MAJOR * DegToRad(lon);
        }

        public double LatToMapX(double lat)
        {
            lat = Math.Min(89.5, Math.Max(lat, -89.5));
            double phi = DegToRad(lat);
            double sinphi = Math.Sin(phi);
            double con = ECCENT * sinphi;
            con = Math.Pow(((1.0 - con) / (1.0 + con)), COM);
            double ts = Math.Tan(0.5 * ((Math.PI * 0.5) - phi)) / con;
            return 0 - EarthRadius_MAJOR * Math.Log(ts);
        }

        public double YToLon(double y)
        {
            return RadToDeg(y) / EarthRadius_MAJOR;
        }

        public double XToLat(double x)
        {
            double ts = Math.Exp(-x / EarthRadius_MAJOR); // ts could mean "true scale"
            double phi = PI_2 - 2 * Math.Atan(ts);
            double dphi = 1.0;
            int i = 0;
            short removeMe = 0;
            while ((Math.Abs(dphi) > 0.000000001) && (i < 15))
            {
                if (removeMe > 100)
                    throw new IndexOutOfRangeException("did not thing this while would take so many loops");
                removeMe++;
                double con = ECCENT * Math.Sin(phi);
                dphi = PI_2 - 2 * Math.Atan(ts * Math.Pow((1.0 - con) / (1.0 + con), COM)) - phi;
                phi += dphi;
                i++;
            }
            return RadToDeg(phi);
        }

        private double RadToDeg(double rad)
        {
            return rad * RAD2Deg;
        }

        private double DegToRad(double deg)
        {
            return deg * DEG2RAD;
        }

        public double ZToLon(double z)
        {
            return YToLon(z);
        }

        public Vector3 PolarToX0Z(double lat, double lon)
        {
            Vector3 result = new Vector3(
                (float)LatToMapX(lat),
                0,
                (float)LonToMapY(lon)
                );
            return result;
        }
    }
}
