﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DataStructures
{
    /// <summary>
    /// Saves all the MapData the device knows.
    /// Implementation might save (part of?) that data to hard-drive or simply keep it in Memory.
    /// Note:   Some mechanism to avoid data-overflow (in memory and on disc) must be implemented. 
    ///         The details of that might need to depend on implementation.
    /// </summary>
    interface IMapDataCombined : IMapDataBlock
    {
        IXmlStringToDataTranslationStrategy TranslatonStrategy { get; }
        /// <summary>
        /// Integrates new MapDataBlocks, avoiding duplicate Nodes. 
        /// </summary>
        /// <param name="additionalMapData"></param>
        void IntegrateNewData(IMapDataBlock additionalMapData);
    }
}
