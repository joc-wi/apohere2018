﻿using ProjectionStrategies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using UnityEngine;

namespace   Deserialisation
{
    /// <summary>
    /// OpenStreetMap-Node
    /// </summary>
    public class OsmNode : OsmBase
    {
        
        //public static implicit operator Vector3 (OsmNode node)
        //{
        //    return new Vector3(node.X, 0, node.Z);
        //}

        private Vector3 _globalPosition;
        public Vector3 GlobalPosition
        {
            get { return _globalPosition; }
        }

        /// <summary>
        /// Id, identical to the one in OpenStreetmap. 
        /// Note: ulong is nice because it is a unsingned int64.
        /// </summary>
        public ulong Id { get; private set; }


        //public float Latitude { get; private set; }
        //public float Longitude { get; private set; }
        public PolarCoordinates PolarCoordinates { get; private set; }

        public float MapSpaceX { get; private set; }
        public float MapSpaceY { get; private set; }

        public OsmNode(XmlNode node, IProjectionStrategy projector)
        {
            Id = GetAttribute<ulong>("id", node.Attributes);

            PolarCoordinates = new PolarCoordinates(GetAttribute<double>("lat", node.Attributes), GetAttribute<double>("lon", node.Attributes));
            MapSpaceY = (float)projector.LonToMapY(PolarCoordinates.lon);
            MapSpaceX = (float)projector.LatToMapX(PolarCoordinates.lat);

            _globalPosition = new Vector3(MapSpaceX, 0, MapSpaceY);
        }

    }
}
