﻿
using ProjectionStrategies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Xml;
using UnityEngine;
using WorldToHexagons;

namespace Deserialisation
{

    /// <summary>
    /// Holding simple and complex data read from xml-strings or deducted from those.
    /// </summary>
    public class MapData 
    {
        


        /// <summary>
        /// All the nodes.
        /// Todo: Should not be in this class.
        /// Note: Having them in a dictionary makes it easy to append map-parts.
        /// Note: ulong is nice because it is a unsingned int64.
        /// </summary>
        internal Dictionary<ulong, OsmNode> Nodes { get; private set; }
        internal Dictionary<ulong, OsmWay> KnownWays { get; private set; }
        internal Dictionary<ulong, OsmWay> NewWays { get; private set; }
        public OsmBounds LatestOsmBounds { get; private set; }

        public MapData()
        {
            Nodes = new Dictionary<ulong, OsmNode>();
            KnownWays = new Dictionary<ulong, OsmWay>();
        }

        public void ExtractData(string mapXml)
        {
            ExtractBasicData(mapXml);
            CreateAdvancedData(LatestOsmBounds, Nodes, NewWays);
        }

        /// <summary>
        /// Extract bounds, nodes and ways out of xml.
        /// </summary>
        /// <param name="mapXml"></param>
        private void ExtractBasicData(string mapXml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(mapXml);
            Debug.Assert(doc.SelectSingleNode("/osm/bounds") != null, "There was no bounds-node within the xml!");

            if (CenterOfUnitySpace.hasBeenSet == false)
            {
                CenterOfUnitySpace.SetCenter(doc.SelectSingleNode("/osm/bounds"));
            }
            SetBounds(doc.SelectSingleNode("/osm/bounds"));
            SetNodes(doc.SelectNodes("/osm/node"));
            SetWays(doc.SelectNodes("/osm/way"));
            //Debug.Log("Some data arrived and was processed.");
            LoadingOutput.Log("Some data arrived and has been processed.");
        }
        
        private void SetBounds(XmlNode xmlNode)
        {
            Debug.Assert(xmlNode != null);
            
            LatestOsmBounds = new OsmBounds(xmlNode);
            
            Debug.Log("Created OsmBounds :" + LatestOsmBounds);
            //Note: the center is important here, not the actual bounds.

        }

        private void SetNodes(XmlNodeList xmlNodeList)
        {
            IProjectionStrategy projector = StrategyServiceProvider.Instance.Projector;
            //todo: try to replace that with a for-loop. Then test the speed-difference in a real scenario.
            // (well, is a simple for-loop even possible on a dict?)
            foreach (XmlNode n in xmlNodeList)
            {
                OsmNode node = new OsmNode(n, projector);
                Nodes[node.Id] = node;
            }
        }

        private void SetWays(XmlNodeList xmlNodeList)
        {
            NewWays = new Dictionary<ulong, OsmWay>();
            foreach(XmlNode node in xmlNodeList)
            {
                OsmWay way = new OsmWay(node, Nodes);
                if (!KnownWays.ContainsKey(way.Id))
                {
                    NewWays[way.Id] = way;
                    KnownWays[way.Id] = way;
                }
                
            }
        }

        
        private void CreateAdvancedData(OsmBounds latestOsmBounds, Dictionary<ulong, OsmNode> nodes, Dictionary<ulong, OsmWay> newWays)
        {
            //Todo: Road-data and House-data has to go here, too.
            WaterArea.UpdateWaterNodes(newWays);
            BuildingArea.UpdateBuildingNodes(newWays);
        }

    }
}
