﻿using Deserialisation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class WaterArea {

    #region static
    public static Dictionary<ulong, WaterArea> waterNodes = new Dictionary<ulong, WaterArea>();

    public static bool IsInWater(PolarCoordinates point)
    {
        foreach (KeyValuePair<ulong, WaterArea> keyValue in waterNodes)
        {
            WaterArea water = keyValue.Value;
            if (water.Contains(point))
            {
                Debug.Log("Point " + point + " is in waterArea " + water.Name + " .");
                return true;
            }
        }
        return false;
    }

    public static void UpdateWaterNodes(Dictionary<ulong, OsmWay> additionalWays)
    {

        foreach (KeyValuePair<ulong, OsmWay> keyValue in additionalWays)
        {
            OsmWay way = keyValue.Value;
            if (way.Tags.ContainsKey("natural") && way.Tags["natural"] == "water" && way.IsClosed)
            {
                waterNodes.Add(keyValue.Key, new WaterAreaClosed(way));
            }
            else if (way.Tags.ContainsKey("natural") && way.Tags["natural"] == "water" && !way.IsClosed)
            {
                waterNodes.Add(keyValue.Key, new WaterAreaLine(way));
            }
            else if (way.Tags.ContainsKey("waterway") && way.IsClosed)
            {
                waterNodes.Add(keyValue.Key, new WaterAreaClosed(way));
            }
            else if (way.Tags.ContainsKey("waterway") && !way.IsClosed)
            {
                waterNodes.Add(keyValue.Key, new WaterAreaLine(way));
            }
        }
    }
    #endregion

    public OsmWay Way { get; protected set; }

    public WaterArea(OsmWay way)
    {
        this.Way = way;
    }
    public string Name {get; protected set;}
    abstract public bool Contains(PolarCoordinates point);
    
}
internal class WaterAreaLine:WaterArea
{
    public WaterAreaLine(OsmWay way):base(way)
    {
        Debug.Assert(!way.IsClosed, "way is closed.");
        if (way.Tags.ContainsKey("waterway"))
        {
            Name = "waterway-" + way.Tags["waterway"] + "_open_" + way.Id;
        }
        else if (way.Tags.ContainsKey("water"))
        {
            Name = "water-" + way.Tags["water"] + "_open_" + way.Id;
        }
        else 
        {
            Name = "water_open_" + way.Id;
        }
    }
    override public bool Contains(PolarCoordinates point)
    {
        float width = 10; //width of the stream in meters. placeholder for now. should be available for each segment.

        float distance = Way.DistanceTo(point);
        
        if (distance < width)
        {
            //Debug.Log("Near Enough: Distance to Line " + this.Name + " is " + distance );
                
            return true;
        }
        
        //Debug.Log("Too far: Distance to Line " + this.Name + " is " + distance );
        return false;
    }
}
internal class WaterAreaClosed:WaterArea
{
    public WaterAreaClosed(OsmWay way) : base(way)
    {
        Debug.Assert(way.IsClosed, "way is not closed.");
        if (way.Tags.ContainsKey("waterway"))
        {
            Name = "waterway-" + way.Tags["waterway"] + "_closed_" + way.Id;
        }
        else if (way.Tags.ContainsKey("water"))
        {
            Name = "water-" + way.Tags["water"] + "_closed_" + way.Id;
        }
        else
        {
            Name = "water_closed_" + way.Id;
        }
    }
    override public bool Contains(PolarCoordinates point)
    {
        if (Way.Contains(point))
        {
            return true;
        }
        return false;
    }
}
