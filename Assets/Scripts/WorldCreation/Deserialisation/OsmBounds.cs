﻿using ProjectionStrategies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using UnityEngine;

namespace Deserialisation
{
    public class OsmBounds : OsmBase
    {


        public PolarCoordinates Min { get; private set; }
        public PolarCoordinates Max { get; private set; }
        public PolarCoordinates AreaCenter { get; private set; }

        //public float MinX { get; private set; }
        //public float MaxX { get; private set; }
        //public float MinY { get; private set; }
        //public float MaxY { get; private set; }

        //public Rect XYRect { get; private set; }

        


        public OsmBounds(XmlNode node)
        {
            Debug.Assert(node != null);
            Debug.Assert(node.Attributes != null);
            Debug.Assert(AttributeExists("minlat", node.Attributes));
            
            float minLat = GetAttribute<float>("minlat", node.Attributes);
            float minLon = GetAttribute<float>("minlon", node.Attributes);
            Min = new PolarCoordinates(minLat, minLon);
            float maxLat = GetAttribute<float>("maxlat", node.Attributes);
            float maxLon = GetAttribute<float>("maxlon", node.Attributes);
            Max = new PolarCoordinates(maxLat, maxLon);

            


            AreaCenter = new PolarCoordinates(0.5 * (minLat + maxLat), 0.5 * (minLon + maxLon));
        }

        public bool Contains(PolarCoordinates coordinate)
        {
            throw new System.NotImplementedException();
        }
        public override string ToString()
        {
            return "OsmBounds(Min=" + Min +", Max=" + Max + ", AreaCenter="+ AreaCenter +")";
        }
    }
}
