﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using UnityEngine;

namespace Deserialisation
{
    public class OsmWay : OsmBase
    {


        public ulong Id { get; private set; }
        public bool Visible { get; private set; }

        public List<OsmNode> Nodes { get; private set; }

        public Dictionary<string, string> Tags { get; private set; }

        public PolarCoordinates WayCenter { get; private set; }
        public float Length { get; private set; }

        public bool IsClosed { get; private set; }
        public bool IsOpen { get { return !IsClosed; } }

        public bool IsHighway { get; private set; }


        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="globalNodeDictionary">Dictionary of all nodes that exist. Usually the one in OsmStringToOsmData.Instance</param>
        public OsmWay(XmlNode node, Dictionary<ulong, OsmNode> globalNodeDictionary)
        {
            Id = GetAttribute<ulong>("id", node.Attributes);

            FillNodes(node, globalNodeDictionary);

            Length = CalculateWayLength();
            WayCenter = CalculateWayCenter();

            SetVisibility(node);

            XmlNodeList xmlTags = node.SelectNodes("tag");
            Tags = new Dictionary<string, string>();
            foreach (XmlNode tag in xmlTags)
            {
                Tags.Add(GetAttribute<string>("k", tag.Attributes), GetAttribute<string>("v", tag.Attributes));
            }



                
            if(Tags.ContainsKey("highway"))
                IsHighway = true;

        }

        /// <summary>
        /// sets NodeIds, Nodes and IsBoundary.
        /// </summary>
        /// <param name="node"></param>
        /// <param name="globalNodeDictionary"></param>
        private void FillNodes(XmlNode node, Dictionary<ulong, OsmNode> globalNodeDictionary)
        {

            Nodes = new List<OsmNode>();
            XmlNodeList nodes = node.SelectNodes("nd");
            foreach (XmlNode n in nodes)
            {
                ulong refNumber = GetAttribute<ulong>("ref", n.Attributes);

                Nodes.Add(globalNodeDictionary[refNumber]);
            }

            // Some things are lines (like roads), some things are boundaries (likebuildings and places):
            if (Nodes.Count > 1)
            {
                IsClosed = (Nodes[0] == Nodes[Nodes.Count - 1]);
            }
        }

        private void SetVisibility(XmlNode node)
        {
            if (AttributeExists("visible", node.Attributes))
                Visible = GetAttribute<bool>("visible", node.Attributes);
            else
                Visible = true;
        }

        
       
        
        private float CalculateWayLength()
        {

            float length = 0;
            if (Nodes.Count == 0)
                throw new System.NotImplementedException();
            if (Nodes.Count == 1)
                return 0;

            for (int i = 1; i < Nodes.Count; i++)
            {
                length += PolarCoordinates.DistanceMeters(Nodes[i].PolarCoordinates, Nodes[i-1].PolarCoordinates);
            }

            return length;
        }

        
        /// <summary>
        /// Does return the (weighted) center of all the nodes of a way.
        /// </summary>
        /// <param name="way"></param>
        /// <returns></returns>
        private PolarCoordinates CalculateWayCenter()
        {
            double totalLat = 0;
            double totalLon = 0;
            if (Nodes.Count == 0)
                throw new System.NotImplementedException();

            int startNode = IsClosed ? 1 : 0;
            for(int i = startNode; i < Nodes.Count;i++)
            {
                totalLat += Nodes[i].PolarCoordinates.lat;
                totalLon += Nodes[i].PolarCoordinates.lon;
            }
            if (IsClosed)
            {
                return new PolarCoordinates(totalLat / (Nodes.Count-1), totalLon / (Nodes.Count-1));
            }
            else
            {
                return new PolarCoordinates(totalLat / Nodes.Count, totalLon / Nodes.Count);
            }
            
        }
        public bool Contains(PolarCoordinates c)
        {
            if (IsOpen)
            {
                return false;
            }

            // the algorithm is called "Jordan" or something. 
            // If you need to cross an uneven number of borders to get to the point, the point is inside the boundaries.
            // I use a way directly from North-pole to point 
            int bordersToCross = 0;
            for (int i = 1; i < Nodes.Count; i++)
            {
                PolarCoordinates a = Nodes[i-1].PolarCoordinates;
                PolarCoordinates b = Nodes[i].PolarCoordinates;
                // easy case: Both Nodes are on the same side of the Northpole-to-c line:
                if ((a.lon < c.lon) == (b.lon < c.lon))
                {
                    continue;
                }
                // easy case: Both Nodes are more south than c:
                if ((a.lat > c.lat) && (b.lat > c.lat))
                {
                    continue;
                }
                else
                {
                    double lamda = (c.lon - a.lon) / (b.lon - a.lon);
                    if( a.lat + lamda * (b.lat - a.lat) < c.lat)
                    {
                        bordersToCross++;
                    }
                }

            }
            return (bordersToCross % 2 == 1); 
        }

        public float DistanceTo(PolarCoordinates point)
        {
            float minDistance = float.MaxValue;
            for (int i = 1; i < Nodes.Count; i++)
            {
                minDistance = Mathf.Min(minDistance, GeoMath.DistanceToLineSegment(point, Nodes[i - 1].PolarCoordinates, Nodes[i].PolarCoordinates));
            }
            return minDistance;
        }
        /// <summary>
        /// Euler angle. just for testing at the moment (though it pobably can be used)
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public float DistanceEulerAngleTo(PolarCoordinates point)
        {
            double minDistance = double.MaxValue;
            for (int i = 1; i < Nodes.Count; i++)
            {
                minDistance = Math.Min(minDistance, GeoMath.EulerAngleToLineSegment(point, Nodes[i - 1].PolarCoordinates, Nodes[i].PolarCoordinates));
            }
            return (float)minDistance;
        }

    }
}
