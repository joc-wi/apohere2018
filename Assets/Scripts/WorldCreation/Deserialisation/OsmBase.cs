﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Deserialisation
{
    /// <summary>
    /// This base-class just provides a few functions for easy access to xml-Data.
    /// Todo: this _needs_ to become a static tool-class! Or better: something that takes the attributes in it's constructor. 
    /// </summary>
    public class OsmBase
    {
        
        protected static T GetAttribute<T>(string attributeName, XmlAttributeCollection attributes)
        {
            string stringValue = attributes[attributeName].Value;
            return (T)Convert.ChangeType(stringValue, typeof(T));
        }
        protected static T GetAttribute<T>(string attributeName, XmlAttributeCollection attributes, int defaultValue)
        {
            T result;
            string stringValue = attributes[attributeName].Value;
            try
            {
                result = (T)Convert.ChangeType(stringValue, typeof(T));
            }
            catch
            {
                result =((T)Convert.ChangeType(defaultValue, typeof(T)));
            }

            return result;

        }


        protected static bool AttributeExists(string attributeName, XmlAttributeCollection attributes)
        {
            return (attributes[attributeName] != null);
        }
        
    }
}
