﻿using Deserialisation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


class BuildingArea
{

    #region static
    public static Dictionary<ulong, BuildingArea> BuildingNodes = new Dictionary<ulong, BuildingArea>();

    public static bool IsInBuilding(PolarCoordinates point)
    {
        foreach (KeyValuePair<ulong, BuildingArea> keyValue in BuildingNodes)
        {
            BuildingArea building = keyValue.Value;
            if (building.Contains(point))
            {
                Debug.Log("Point " + point + " is in BuildingArea " + building.Name + " .");
                return true;
            }
        }
        return false;
    }

    public static void UpdateBuildingNodes(Dictionary<ulong, OsmWay> additionalWays)
    {

        foreach (KeyValuePair<ulong, OsmWay> keyValue in additionalWays)
        {
            OsmWay way = keyValue.Value;
            if (!way.Tags.ContainsKey("building"))
            {
                continue;
            }
            if (!way.IsClosed)
            {
                Debug.Log("Odd. Way " + way.Id + " is a building that is not closed. Is it a wall?");
                continue;
            }
            if (way.Tags["building"] == "roof")
                continue;
            if (way.Tags["building"] == "static_caravan")
                continue;
            BuildingNodes.Add(keyValue.Key, new BuildingArea(way));

        }
    }
    #endregion




    public OsmWay Way { get; protected set; }

    // bundle the following in BuildingAttributes or BoundaryAttributes:
    public byte HasPersonalStuff { get; private set; }
    public byte HasToolStuff { get; private set; }
    public byte HasMachineStuff { get; private set; }
    public byte HasFarmStuff { get; private set; }
    public byte HasCarStuff { get; private set; }

    public float Height { get; private set; }




    public BuildingArea(OsmWay way)
    {
        this.Way = way;
        Name = "Building-" + way.Id;
        breakDownBuildingTag(way.Tags["building"]);

    }
    public string Name { get; protected set; }
    public bool Contains(PolarCoordinates point)
    {
        if (Way.Contains(point))
        {
            return true;
        }
        return false;
    }
    private void breakDownBuildingTag(string value)
    {
        /// See here: https://taginfo.openstreetmap.org/keys/building#values
        switch (value)
        {
            case "yes":
                break;
            case "house":
                break;
            case "residential":
                HasPersonalStuff = 100;
                break;
            case "garage":
                HasCarStuff = 100;
                HasToolStuff = 20;
                break;
            case "apartments":
                HasPersonalStuff = 100;
                break;
            case "hut":
                break;
            case "industrial":
                HasMachineStuff = 100;
                HasToolStuff = 50;
                break;
            case "detached":
                break;
            case "shed":
                HasToolStuff = 20;
                break;
            //case "roof":
            //    IsBuilding = false;
            //    break;
            case "commercial":
                HasMachineStuff = 20;
                HasToolStuff = 20;
                break;
            case "garages":
                HasCarStuff = 100;
                HasToolStuff = 20;
                break;
            case "terrace":
                HasPersonalStuff = 100;
                break;
            case "school":
                break;
            case "retail":
                break;
            case "greenhouse":
                HasFarmStuff = 50;
                break;
            case "construction":
                HasToolStuff = 50;
                break;
            case "farm_auxiliary":
                HasToolStuff = 10;
                HasFarmStuff = 100;
                break;
            case "church":
                break;
            case "barn":
                HasFarmStuff = 100;
                break;
            case "warehouse":
                HasMachineStuff = 20;
                break;
            case "service":
                HasToolStuff = 20;
                break;
            case "cabin":
                break;
            case "farm":
                HasFarmStuff = 100;
                break;
            case "civic":
                break;
            case "manufacture":
                HasMachineStuff = 50;
                HasToolStuff = 50;
                break;
            case "office":
                break;
            case "university":
                break;
            //case "static_caravan":
            //    IsBuilding = false;
            //    HasCarStuff = 100;
            //    break;
            case "collapsed":
                break;
            case "public":
                break;
            case "hospital":
                break;
            case "hangar":
                HasToolStuff = 20;
                break;
            case "hotel":
                break;
            case "chapel":
                break;
            case "semidetached_house":
                break;
            case "ger":
                break;
            case "bungalow":
                break;
            case "entrance":
                break;
            case "kindergarden":
                break;
            case "ruins":
                break;
            case "mosque":
                break;
            case "storage_tank":
                break;
            case "train_station":
                break;
            case "transportation":
                break;
            case "damaged":
                break;
            case "stable":
                HasFarmStuff = 100;
                break;
            default:
                Debug.LogWarning("I did not recognise building type " + value + ".");
                break;

        }
    }

}

