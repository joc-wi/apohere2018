﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GeometyCreation
{
    /// <summary>
    /// This is a helper-class when working with a runtime-generated mesh for a mesh-renderer.
    /// </summary>
    internal class MeshHelper
    {
        public List<Vector3> VertexList { get; private set; }
        public List<Vector3> NormalList { get; private set; }
        public List<Vector2> UvList { get; private set; }
        private Vector3 defaultNormal;

        internal MeshHelper(Vector3 defaultNormal)
        {
            this.defaultNormal = defaultNormal;
            VertexList = new List<Vector3>();
            NormalList = new List<Vector3>();
            UvList = new List<Vector2>();
        }

        /// <summary>
        /// This will add a vertex to the given lists.
        /// </summary>
        /// <returns>Index of the vertex in the lists.</returns>
        public int AddVertexToList(Vector3 vertexPosition, Vector2 vertexUv, Vector3 vertexNormal)
        {
            Debug.Assert(VertexList.Count == NormalList.Count); // Todo: remove this. Long term. Perhaps.
            VertexList.Add(vertexPosition);
            NormalList.Add(vertexNormal);
            UvList.Add(vertexUv);
            return VertexList.Count - 1;
        }

        public int AddVertexToList(Vector3 vertexPosition, Vector2 vertexUv)
        {
            return AddVertexToList(vertexPosition, vertexUv, defaultNormal);
        }
    }
}
