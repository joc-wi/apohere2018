﻿using Deserialisation;
using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using WorldToHexagons;


namespace GeometyCreation
{
    class WaterMaker : GeometryMakerBehavior
    {
        

        [UsedImplicitly]
        [SerializeField]
        private Material waterMaterial;
 
        [UsedImplicitly]
        [SerializeField]
        private float privateHeight = -0.2f;

        override public void CreateGeometry(OsmBounds currentBounds, Dictionary<ulong, OsmNode> allNodes, Dictionary<ulong, OsmWay> additionalWays, List<HexCenterPoint> newHexCenters)
        {
            
            CreateParentGroup("GroundHexes");
            int numberOfNewHexes = 0;
            
            foreach (HexCenterPoint hexCenterPoint in newHexCenters)
            {
                //Debug.Log("Creating hexagonGeometry for " + hexCenterPoint.PolarCoordinates);
                GameObject hexGeo = CreateHexGeometry(currentBounds, hexCenterPoint);


                hexGeo.transform.SetParent(parentGroup);
                numberOfNewHexes++;


            }
            //string debugString = "GroundMaker processed Hexes.";
            //debugString += "Number of new Hexes (that were used to create geometry):" + numberOfNewHexes + "\n Number of old Hexes that where ignored: " + numberOfIgnoredHexes;
            //debugString += "\n Old Hexes: ";

            //Debug.Log(debugString);
        }

        

        private GameObject CreateHexGeometry(OsmBounds currentBounds, HexCenterPoint hexCenterPoint)
        {

            //Debug.Log("starting to createHexGeometry");
            GameObject HexGeometry = new GameObject("Hex_" + hexCenterPoint.Id);
            HexGeometry.transform.SetParent(this.transform);
            Vector3 originUnitySpace = hexCenterPoint.PolarCoordinates.UnitySpace;
            //Debug.Log("CreateHexGeometry: will place this hex at " + originUnitySpace + " in Unity-space.");
            HexGeometry.transform.position = originUnitySpace;
            MeshFilter meshFilter = HexGeometry.AddComponent<MeshFilter>();

            HexGeometry.AddComponent<MeshRenderer>();
            HexGeometry.GetComponent<Renderer>().material = waterMaterial;

            MeshHelper meshHelper = new MeshHelper(Vector3.up);

            Vector3 faceNormalOfAllVertices = Vector3.up;

            // Add the center Point:
            meshHelper.AddVertexToList(Vector3.zero, new Vector2(0.5f, 0.5f));


            //Debug.Log("Will start the for-loop. expecting " + hexCenterPoint.Neighbours.Count + " elements.");

            for (int i = 0; i < hexCenterPoint.Neighbours.Count; i++)
            {
                HexCenterPoint borderHexA = hexCenterPoint.Neighbours[i];
                HexCenterPoint borderHexB = hexCenterPoint.Neighbours[(i + 1) % hexCenterPoint.Neighbours.Count];

                Vector3 borderPoint = BorderPoint(hexCenterPoint, borderHexA, borderHexB, currentBounds, privateHeight);
                meshHelper.AddVertexToList(borderPoint, new Vector2(0f, 0.5f));
            }
            meshFilter.mesh.vertices = meshHelper.VertexList.ToArray();
            meshFilter.mesh.normals = meshHelper.NormalList.ToArray();
            meshFilter.mesh.uv = meshHelper.UvList.ToArray();
            if (hexCenterPoint.Neighbours.Count == 6)
            {
                int[] indicesForTriangles = {
                    0,2,1,
                    0,3,2,
                    0,4,3,
                    0,5,4,
                    0,6,5,
                    0,1,6
                };
                meshFilter.mesh.triangles = indicesForTriangles;
            }
            else if (hexCenterPoint.Neighbours.Count == 5)
            {
                int[] indicesForTriangles = {
                    0,2,1,
                    0,3,2,
                    0,4,3,
                    0,5,4,
                    0,1,5
                };
                meshFilter.mesh.triangles = indicesForTriangles;
            }
            else { throw new System.IndexOutOfRangeException(); }
            return HexGeometry;
            //Debug.Log(
            //    "meshFilter.mesh.vertices = " + meshFilter.mesh.vertices + "\n" +
            //    "meshFilter.mesh.normals = " + meshFilter.mesh.normals + "\n" +
            //    "meshFilter.mesh.uv = " + meshFilter.mesh.uv + "\n" +
            //    "meshFilter.mesh.triangles = " + meshFilter.mesh.triangles 
            //    );
        }

       
        /// <summary>
        /// The point where three hexes meet, measured from the first
        /// </summary>
        private Vector3 BorderPoint(HexCenterPoint center, HexCenterPoint neighbourA, HexCenterPoint neighbourB, OsmBounds currentBounds, float height)
        {
            Vector3 posCenter = center.PolarCoordinates.UnitySpace;
            Vector3 posNeighbourA = neighbourA.PolarCoordinates.UnitySpace;
            Vector3 posNeighbourB = neighbourB.PolarCoordinates.UnitySpace;
            return height*Vector3.up + (posCenter + posNeighbourA + posNeighbourB) / 3f - posCenter;
        }
    }
}