﻿using Deserialisation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using WallGeneration;
using WorldToHexagons;

namespace GeometyCreation
{ 
    
    class BuildingMakerNiceWall : GeometryMakerBehavior
    {
        [SerializeField]
        private Wall wallPrefab;
        //Material buildingMaterial;
        
       
        override public void CreateGeometry(OsmBounds currentBounds, Dictionary<ulong, OsmNode> allNodes, Dictionary<ulong,OsmWay>  additionalWays, List<HexCenterPoint> hexCenters)
        {
            Debug.Log("BuildingMaker.CreateGeometry() has been called.");

            CreateParentGroup("Buildings");

            foreach (KeyValuePair<ulong, BuildingArea> keyWayPair in BuildingArea.BuildingNodes)
            {
                createBuildingGeometry(keyWayPair.Value);
            }

        }

        


        private void createBuildingGeometry(BuildingArea buildingArea)
        {
            //Debug.Log("creating a building.");
            GameObject buildingGeometry = new GameObject(buildingArea.Name);
            buildingGeometry.transform.SetParent(parentGroup);
            //Vector3 buildingOriginLocal = GetCenterGlobal(way) - CenterOfUnitySpace.CenterMapSpace;

            buildingGeometry.transform.position = buildingArea.Way.WayCenter.UnitySpace;

            //Vector3 heightVector = new Vector3(0, way.Height, 0);
            //int levels = (int)(way.Height / 3f); //Todo: that is a bullshit approximation.


            for (int i = 1; i < buildingArea.Way.Nodes.Count; i++)
            {
                OsmNode node1 = buildingArea.Way.Nodes[i - 1];
                OsmNode node2 = buildingArea.Way.Nodes[i];

                Vector3 startPoint = node1.PolarCoordinates.UnitySpace - buildingArea.Way.WayCenter.UnitySpace;
                Vector3 endPoint = node2.PolarCoordinates.UnitySpace - buildingArea.Way.WayCenter.UnitySpace;

                SetWall(startPoint, endPoint, buildingGeometry);
                
            }
            
        }
        private void SetWall(Vector3 startPoint, Vector3 endPoint, GameObject parent)
        {
            Wall wall = GameObject.Instantiate<Wall>(wallPrefab, parent.transform);
            wall.SetWall(startPoint, endPoint, 5);
        }
    }
}
