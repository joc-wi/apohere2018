﻿using Deserialisation;
using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using WorldToHexagons;

namespace GeometyCreation
{
    
    class LineOverlayMaker : GeometryMakerBehavior
    {
        public float height = 10f;
        [SerializeField]private LineRenderer lineRendererPrefab;

        private Transform group;
        override public void CreateGeometry(OsmBounds currentBounds, Dictionary<ulong, OsmNode> allNodes, Dictionary<ulong, OsmWay> additionalWays, List<HexCenterPoint> newHexCenters)
        {
            group = new GameObject().transform;
            group.parent = transform;
            group.name = "Line-Map";
            foreach(KeyValuePair<ulong, WaterArea> keyValuePair in WaterArea.waterNodes){
                Draw(keyValuePair.Value);
            }
        }

        private void Draw(WaterArea waterArea)
        {
            List<OsmNode> nodes = waterArea.Way.Nodes;
            LineRenderer lineRenderer = Instantiate(lineRendererPrefab, group);
            lineRenderer.transform.position = Vector3.zero;
            lineRenderer.name = waterArea.Name;
            lineRenderer.positionCount =nodes.Count;
            //Debug.Log("nodes.Count = " + nodes.Count + ", lineRenderer.Positions.Count = " + lineRenderer.Positions.count);
            for(int i=0; i< lineRenderer.positionCount; i++)
            {
                //Debug.Log("i = " + i);
                lineRenderer.SetPosition(i, nodes[i].PolarCoordinates.UnitySpace + height * Vector3.up);
            }
            if (waterArea.Way.IsClosed)
            {
                //lineRenderer.startColor = Color.blue;
                //lineRenderer.endColor = Color.blue;
                lineRenderer.material.color = Color.blue;
            }
            else
            {
                lineRenderer.material.color = Color.cyan;
            }
            
        }
    }
}