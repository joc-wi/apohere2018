﻿using Deserialisation;
using System.Collections.Generic;
using UnityEngine;
using WorldToHexagons;

namespace GeometyCreation
{
    

    abstract internal class GeometryMakerBehavior:MonoBehaviour
    {
        [SerializeField]
        private bool preventActivation = false;

        protected Transform parentGroup;

        protected virtual void Awake()
        {
            if (preventActivation)
                return;
            //Debug.Log("GeometryMakerBehavior.Awake() just got called. Should subscribe to dataProvider.CreateGeometryEvent");

            RequestRailroad.CreatingGeometryEvent += CreateGeometry;
        }
        

        abstract public void CreateGeometry
            (
            OsmBounds currentBounds, 
            Dictionary<ulong, OsmNode> allNodes, 
            Dictionary<ulong,OsmWay> additionalWays, 
            List<HexCenterPoint> hexCenters
            );

        public void CreateParentGroup(string name)
        {
            parentGroup = new GameObject(name).transform;
            parentGroup.SetParent(gameObject.transform);
        }
    }
}