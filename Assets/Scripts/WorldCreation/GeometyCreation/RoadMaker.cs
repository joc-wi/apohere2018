﻿using Deserialisation;
using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using WorldToHexagons;

namespace GeometyCreation
{
    
    class RoadMaker:GeometryMakerBehavior
    {
        [SerializeField] private Road roadPrefab;
        
        
        
        
        override public void CreateGeometry(OsmBounds currentBounds, Dictionary<ulong, OsmNode> allNodes, Dictionary<ulong,OsmWay> additionalWays, List<HexCenterPoint> hexCenters)
        {
            CreateParentGroup("Roads");
            foreach (KeyValuePair<ulong, OsmWay> keyWayPair in additionalWays)
            {
                if (keyWayPair.Value.IsHighway)
                {
                    // todo: the following should be bundled it a separate Class:
                    CreateRoad(currentBounds, keyWayPair.Value); 
                }   
            }
        }
        

        private void CreateRoad(OsmBounds currentBounds, OsmWay way)
        {
            Road road = Instantiate<Road>(roadPrefab);
            road.transform.SetParent(parentGroup);
            road.transform.position = way.WayCenter.UnitySpace;

            road.Init(currentBounds, way);

            //CreateRoadGeometry(currentBounds, way);
            
        }  
    }
}
