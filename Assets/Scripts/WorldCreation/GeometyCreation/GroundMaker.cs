﻿using Deserialisation;
using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using WorldToHexagons;

namespace GeometyCreation
{
    class GroundMaker : GeometryMakerBehavior
    {
        [UsedImplicitly][SerializeField]
        private Material groundMaterial;
        [UsedImplicitly]
        [SerializeField]
        private Material outlineMaterial;
        [UsedImplicitly]
        [SerializeField]
        private float outlineHeight = 0.1f;

        override public void CreateGeometry(OsmBounds currentBounds, Dictionary<ulong, OsmNode> allNodes, Dictionary<ulong, OsmWay> additionalWays, List<HexCenterPoint> newHexCenters)
        {
            Debug.Log("GroundMaker.CreateGeometry() has been called. newHexCenters.Count = " + newHexCenters.Count);            
            
            LoadingOutput.Log("Creating Ground...");
            CreateParentGroup("GroundHexes");
            int numberOfNewHexes=0;

            foreach (HexCenterPoint hexCenterPoint in newHexCenters)
            {
                //Debug.Log("Creating hexagonGeometry for " + hexCenterPoint.PolarCoordinates);
                GameObject hexGeo = CreateSingleHexGeometry(hexCenterPoint);
                GameObject hexOutline = CreateHexOutline(currentBounds, hexCenterPoint);
                hexOutline.transform.parent = hexGeo.transform;
                hexGeo.transform.SetParent(parentGroup);
                numberOfNewHexes++;
                
                
            }
            //string debugString = "GroundMaker processed Hexes.";
            //debugString += "Number of new Hexes (that were used to create geometry):" + numberOfNewHexes + "\n Number of old Hexes that where ignored: " + numberOfIgnoredHexes;
            //debugString += "\n Old Hexes: ";
            
            //Debug.Log(debugString);
        }

        private GameObject CreateSingleHexGeometry( HexCenterPoint hexCenterPoint)
        {
            
            //Debug.Log("starting to createHexGeometry");
            GameObject HexGeometry = new GameObject("Hex_" + hexCenterPoint.Id);

            MeshFilter meshFilter = PrepareGameObject(HexGeometry, hexCenterPoint);
            CreateVertices(meshFilter, hexCenterPoint);

            return HexGeometry;
            //Debug.Log(
            //    "meshFilter.mesh.vertices = " + meshFilter.mesh.vertices + "\n" +
            //    "meshFilter.mesh.normals = " + meshFilter.mesh.normals + "\n" +
            //    "meshFilter.mesh.uv = " + meshFilter.mesh.uv + "\n" +
            //    "meshFilter.mesh.triangles = " + meshFilter.mesh.triangles 
            //    );
        }
        private MeshFilter PrepareGameObject(GameObject HexGeometry, HexCenterPoint hexCenterPoint)
        {
            HexGeometry.transform.SetParent(this.transform);
            Vector3 originUnitySpace = hexCenterPoint.PolarCoordinates.UnitySpace;
            //Debug.Log("CreateHexGeometry: will place this hex at " + originUnitySpace + " in Unity-space.");
            HexGeometry.transform.position = originUnitySpace;
            MeshFilter meshFilter = HexGeometry.AddComponent<MeshFilter>();

            HexGeometry.AddComponent<MeshRenderer>();
            HexGeometry.GetComponent<Renderer>().material = groundMaterial;
            return meshFilter;
        }
        private void CreateVertices(MeshFilter meshFilter, HexCenterPoint hexCenterPoint)
        {
            MeshHelper meshHelper = new MeshHelper(Vector3.up);
            // Add the center Point:
            meshHelper.AddVertexToList(CenterPoint(hexCenterPoint), new Vector2(0.5f, 0.5f));

            for (int i = 0; i < hexCenterPoint.Neighbours.Count; i++)
            {
                HexCenterPoint borderHexA = hexCenterPoint.Neighbours[i];
                HexCenterPoint borderHexB = hexCenterPoint.Neighbours[(i + 1) % hexCenterPoint.Neighbours.Count];

                Vector3 borderPoint = BorderPoint(hexCenterPoint, borderHexA, borderHexB);
                meshHelper.AddVertexToList(borderPoint, new Vector2(0f, 0.5f));
            }
            meshFilter.mesh.vertices = meshHelper.VertexList.ToArray();
            meshFilter.mesh.normals = meshHelper.NormalList.ToArray();
            meshFilter.mesh.uv = meshHelper.UvList.ToArray();
            if (hexCenterPoint.Neighbours.Count == 6)
            {
                int[] indicesForTriangles = {
                    0,2,1,
                    0,3,2,
                    0,4,3,
                    0,5,4,
                    0,6,5,
                    0,1,6
                };
                meshFilter.mesh.triangles = indicesForTriangles;
            }
            else if (hexCenterPoint.Neighbours.Count == 5)
            {
                int[] indicesForTriangles = {
                    0,2,1,
                    0,3,2,
                    0,4,3,
                    0,5,4,
                    0,1,5
                };
                meshFilter.mesh.triangles = indicesForTriangles;
            }
            
        }

        private GameObject CreateHexOutline(OsmBounds currentBounds, HexCenterPoint hexCenterPoint)
        {
            //Debug.Log("starting to createHexOutline");
            GameObject HexOutline = new GameObject("HexOutline_" + hexCenterPoint.Id);
            HexOutline.transform.SetParent(this.transform);
            Vector3 originUnitySpace = hexCenterPoint.PolarCoordinates.UnitySpace;

            HexOutline.transform.position = originUnitySpace;
            MeshFilter meshFilter = HexOutline.AddComponent<MeshFilter>();

            HexOutline.AddComponent<MeshRenderer>();
            HexOutline.GetComponent<Renderer>().material = outlineMaterial;


            MeshHelper meshHelper = new MeshHelper(Vector3.up);



            Vector3 faceNormalOfAllVertices = Vector3.up;



            //Debug.Log("Will start the for-loop. expecting " + hexCenterPoint.Neighbours.Count + " elements.");


            for (int i = 0; i < hexCenterPoint.Neighbours.Count; i++)
            {
                HexCenterPoint borderHexA = hexCenterPoint.Neighbours[i];
                HexCenterPoint borderHexB = hexCenterPoint.Neighbours[(i + 1) % hexCenterPoint.Neighbours.Count];

                Vector3 borderPoint = BorderPoint(hexCenterPoint, borderHexA, borderHexB);
                Vector3 innerBorderPoint = (borderPoint * 99 + Vector3.zero) * 0.01f;
                borderPoint += Vector3.up * outlineHeight;
                innerBorderPoint += Vector3.up * outlineHeight;
                meshHelper.AddVertexToList(borderPoint, new Vector2(0f, 0.5f));
                meshHelper.AddVertexToList(innerBorderPoint, new Vector2(0f, 0.5f));
            }
            meshFilter.mesh.vertices = meshHelper.VertexList.ToArray();
            meshFilter.mesh.normals = meshHelper.NormalList.ToArray();
            meshFilter.mesh.uv = meshHelper.UvList.ToArray();

            if (hexCenterPoint.Neighbours.Count == 6)
            {
                int[] indicesForTriangles = {
                    0,1,2,
                    2,1,3,
                    2,3,4,
                    4,3,5,
                    4,5,6,
                    6,5,7,
                    6,7,8,
                    8,7,9,
                    8,9,10,
                    10,9,11,
                    10,11,0,
                    0,11,1,
                };
                meshFilter.mesh.triangles = indicesForTriangles;
            }
            else if (hexCenterPoint.Neighbours.Count == 5)
            {
                int[] indicesForTriangles = {
                    0,1,2,
                    2,1,3,
                    2,3,4,
                    4,3,5,
                    4,5,1
                };
                meshFilter.mesh.triangles = indicesForTriangles;
            }
            else { throw new System.IndexOutOfRangeException(); }

            return HexOutline;
            //Debug.Log(
            //    "meshFilter.mesh.vertices = " + meshFilter.mesh.vertices + "\n" +
            //    "meshFilter.mesh.normals = " + meshFilter.mesh.normals + "\n" +
            //    "meshFilter.mesh.uv = " + meshFilter.mesh.uv + "\n" +
            //    "meshFilter.mesh.triangles = " + meshFilter.mesh.triangles
            //    );
        }

        private Vector3 CenterPoint(HexCenterPoint center)
        {
            //Debug.Log("Now testing center point");
            Vector3 heightOffset = HeightOffset(center.PolarCoordinates);
            return  heightOffset;
        }

        /// <summary>
        /// The point where three hexes meet, measured from the first
        /// </summary>
        private Vector3 BorderPoint(HexCenterPoint center, HexCenterPoint neighbourA, HexCenterPoint neighbourB)
        {
            
            Vector3 posCenter = center.PolarCoordinates.UnitySpace;
            Vector3 posNeighbourA = neighbourA.PolarCoordinates.UnitySpace;
            Vector3 posNeighbourB = neighbourB.PolarCoordinates.UnitySpace;
            PolarCoordinates point = PolarCoordinates.UnitySpaceToPolar((posCenter + posNeighbourA + posNeighbourB) / 3f);
            Vector3 heightOffset = HeightOffset(point);
            return  point.UnitySpace - posCenter + heightOffset;
        }
        private Vector3 HeightOffset(PolarCoordinates point)
        {
            return WaterArea.IsInWater(point) ? (Vector3.up * -2.5f) : Vector3.zero;
        }
    }
}
