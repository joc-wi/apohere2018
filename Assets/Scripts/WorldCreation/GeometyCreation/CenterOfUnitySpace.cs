﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using UnityEngine;

namespace Deserialisation
{
    /// <summary>
    /// Todo: this should perhaps be a static part of PolarCoordinates.
    /// Todo: this should work with double-precision. As well as any function using it.
    /// Todo: Introduce a double3 and a double2 to deal with the math involved here.
    /// </summary>
    class CenterOfUnitySpace : OsmBase
    {
        public static bool hasBeenSet { get; private set; }
        public static void SetCenter(XmlNode node)
        {
            Debug.Assert(node != null);
            Debug.Assert(node.Attributes != null);
            Debug.Assert(AttributeExists("minlat", node.Attributes));

            float minLat = GetAttribute<float>("minlat", node.Attributes);
            float minLon = GetAttribute<float>("minlon", node.Attributes);

            float maxLat = GetAttribute<float>("maxlat", node.Attributes);
            float maxLon = GetAttribute<float>("maxlon", node.Attributes);

            SetCenter(minLat, minLon, maxLat, maxLon);
        }
        public static void SetCenter(double minLat, double minLon, double maxLat, double maxLon)
        {
            PolarCoordinates Min = new PolarCoordinates(minLat, minLon);
            PolarCoordinates Max = new PolarCoordinates(maxLat, maxLon);

            CenterMapSpace = 0.5f * new Vector3(Min.MapSpace.x + Max.MapSpace.x, 0, Min.MapSpace.y + Max.MapSpace.y);
            hasBeenSet = true;
        }

        private static Vector3 __centerMapSpace;
        /// <summary>
        /// What is this: It is the StartPosition, not the current Player-Position (at the moment, at least).
        /// What Space is it in: This is in 3d coordinates. But since it is a flat map, y is always 0. Origin is the position on  0°,0°.
        /// What scale is it in: Meter.
        /// What is it used for: This will be used as the origin for local Unity coordinates.
        /// 
        /// </summary>
        public static Vector3 CenterMapSpace
        {
            get
            {
                if (!hasBeenSet)
                {
                    throw new System.Exception();
                }
                return __centerMapSpace;
            }
            set
            {
                if (hasBeenSet)
                {
                    throw new System.Exception("The Center has already been set. \n (this has to become more flexible, but for not the center cannot be reset)");
                }
                __centerMapSpace = value;
            }
        }
    }
}
