﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProvideMapFromWeb :  ProvideMapStringBase

{



    public override IEnumerator StartGettingMapData(PolarCoordinates lowerLeft, PolarCoordinates upperRight, Action<bool, string> functionToCallOnResult)
    {
        IsWaitingForMap = true;

        string url = BuildUrl(lowerLeft, upperRight);

        yield return GetMapData(url,functionToCallOnResult);
    }
    /// <summary>
    /// returns something like "https://overpass-api.de/api/map?bbox=7.72053,47.88766,7.72474,47.88926"
    /// </summary>
    /// <returns></returns>
    private string BuildUrl(PolarCoordinates lowerLeft, PolarCoordinates upperRight)
    {
        // It seems like the api is an asshole and breaks with "lat before lon"-rule!
        string url = "https://overpass-api.de/api/map?bbox=" + 
        lowerLeft.lon.ToString("G", System.Globalization.CultureInfo.InvariantCulture) + 
        "," + 
        lowerLeft.lat.ToString("G", System.Globalization.CultureInfo.InvariantCulture) +
        "," + 
        upperRight.lon.ToString("G", System.Globalization.CultureInfo.InvariantCulture) + 
        "," + 
        upperRight.lat.ToString("G", System.Globalization.CultureInfo.InvariantCulture);
        Debug.Log("url: " + url);
        return url;
    }

    IEnumerator GetMapData(string url, Action<bool, string> functionToCallOnResult)
    {

        float starttime = Time.time;
        using (WWW www = new WWW(url))
        {
            StartCoroutine(ShowProgress(www));
            yield return www;
            Debug.Log("Got Map Data! Loading time: " + (Time.time - starttime));
            
            functionToCallOnResult(true, www.text);
            IsWaitingForMap = false;
        }
    }

    private IEnumerator ShowProgress(WWW www)
    {
        LoadingOutput.Log("Sending request to server.");
        while (www.uploadProgress < 1)
        {
            MapRequester.ShowLoadingProgress(www.uploadProgress * 0.1f, "Requesting Data from Server... ");
            
            yield return null;
        }
        LoadingOutput.Log("Request to Server sent. Waiting for answer");
        while (www.progress == 0)
        {
            MapRequester.ShowLoadingProgress(0.1f, "Waiting for Server-Answer... ");
            yield return null;
        }
        LoadingOutput.Log("Recieving Server-answer...");
        while (www.progress < 1)
        {
            MapRequester.ShowLoadingProgress(0.1f +www.progress * 0.8f, "Receiving Map-Data from Server... ");
            yield return null;
        }
        LoadingOutput.Log("Server-answer recieved.");
    }
}
