﻿using ProjectionStrategies;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapRequester : MonoBehaviour {


    // Todo: The functionality of this class should be moved somewhere sensible. 
    //          Perhaps the loading-screen? (it might not even need the event anymore...)
    //          Perhaps RequestRailroad? (that class is a bit long already...)

    public static event System.Action<float, string> ShowProgressEvent;
    public static void ShowLoadingProgress(float progress, string message = "Loading...")
    {
        if(ShowProgressEvent != null)
        {
            ShowProgressEvent(progress, message);
        }
        
    }
}
