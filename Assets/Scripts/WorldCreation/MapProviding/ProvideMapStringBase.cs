﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;

/// <summary>
/// Provides xml-formated strings that contain map-data.
/// Different implementation may get this data using different means.
/// Usually, an implementations might get the data be from some kind of server.
/// Implementations for Test-Scenarios might generate the string themselves
/// </summary>
public abstract class ProvideMapStringBase : MonoBehaviour  {
    
    public bool IsWaitingForMap { get; protected set; }


    /// <summary>
    /// Sends a request to get map-data of the specified area.
    /// Does _not_ return this data directly, since it may have to wait for 
    /// the answer of a server. The requested data (if it can be aquired) 
    /// will be returned by the OnNewMapData event.
    /// 
    /// </summary>
    /// <param name="lowerLeft"></param>
    /// <param name="upperRight"></param>
    abstract public IEnumerator StartGettingMapData(PolarCoordinates lowerLeft, PolarCoordinates upperRight, Action<bool, string> functionToCallOnResult);

    
}
