﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;


/// <summary>
/// A test-implementation of IProvideMapString.
/// It simply reads the Map-String out of a file, instead of requesting it in the internet.
/// </summary>
class ProvideMapFromFile:  ProvideMapStringBase
{
    [SerializeField]
    private string fileName;
    private string textInFile;

    /// <summary>
    /// Sends a request to get map-data of the specified area.
    /// Does _not_ return this data directly, since it may have to wait for 
    /// the answer of a server. The requested data (if it can be aquired) 
    /// will be returned by the OnNewMapData event.
    /// 
    /// </summary>
    /// <param name="lowerLeft"></param>
    /// <param name="upperRight"></param>
    public override IEnumerator StartGettingMapData(PolarCoordinates lowerLeft, PolarCoordinates upperRight, Action<bool, string> functionToCallOnResult)
    {
        
        //Debug.Log("ProvideMapFromFile.StartGettingMapData(...)");
        IsWaitingForMap = true;
        readFile();
        //Debug.Log("File has been read.");
        
        yield return new WaitForSeconds(1);
        //no matter what you asked for, this class will always return the same string from the same file:
        functionToCallOnResult(true, textInFile);
        IsWaitingForMap = false;
    }

    

    /// <summary>
    /// Stores the content of the file in the variable textInFile. 
    /// Code taken mostly from a user named "Drakestar", found here: http://answers.unity3d.com/questions/279750/loading-data-from-a-txt-file-c.html
    /// I have to admit that his code is a bit more complicated, checking each line and stuff.
    /// </summary>
    /// <returns>Returns false if reading the file failed.</returns>
    private bool readFile()
    {
        
        // Handle any problems that might arise when reading the text
        try
        {
            // Create a new StreamReader, tell it which file to read and what encoding the file
            // was saved as
            StreamReader theReader = new StreamReader(fileName, Encoding.Default);
            // Immediately clean up the reader after this block of code is done.
            // You generally use the "using" statement for potentially memory-intensive objects
            // instead of relying on garbage collection.
            // (Do not confuse this with the using directive for namespace at the 
            // beginning of a class!)
            using (theReader)
            {
                textInFile = theReader.ReadToEnd();
                // Done reading, close the reader and return true to broadcast success    
                theReader.Close();
                return true;
            }
        }

        catch (Exception e)
        {
            Debug.LogError("Could not read File:\n" + e.Message);
            textInFile = String.Empty;
            return false;
        }
    }
}



