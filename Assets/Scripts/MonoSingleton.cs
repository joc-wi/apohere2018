﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>{
    static private T instance;
    static public T Instance {
        get {
            if (instance == null)
            {
                Debug.LogError("Instance of " + typeof(T) +" is null. Either you are trying to access it during awake, or you do not have any instance in the scene! \n Try ");
            }
            return instance;
        }
    }

    protected virtual void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("Trying to create a second instance of this singleton!");
            return;
        }
        instance = (T)this;
    }
    
	
}
