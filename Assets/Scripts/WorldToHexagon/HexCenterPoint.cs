﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WorldToHexagons
{


    public class HexCenterPoint
    {

        public static string GetMidpointId(HexCenterPoint centerA, HexCenterPoint centerB) //Todo: no need to have this static, right?
        {
            string newId;
            //if (centerA.Id < centerB.Id)
            if (String.Compare(centerA.Id,centerB.Id, StringComparison.InvariantCultureIgnoreCase)<0)
                {
                int positionOfOtherInClockwiseNeighbours = centerA.GetPositionInClockwiseNeighbours(centerB);
                // asumes that this parent is the one with lower Id:
                //newId = centerA.Id * 10 + positionOfOtherInClockwiseNeighbours + 1;
                newId = centerA.Id + (positionOfOtherInClockwiseNeighbours + 1);
            }
            else
            {
                int positionOfOtherInClockwiseNeighbours = centerB.GetPositionInClockwiseNeighbours(centerA);
                // asumes that this parent is the one with lower Id:
                //newId = centerB.Id * 10 + positionOfOtherInClockwiseNeighbours + 1;
                newId = centerB.Id + (positionOfOtherInClockwiseNeighbours + 1);
            }
            return newId;
        }

        public string Id { get; private set; }
        public int Generation { get; private set; }
        public string Name { get; private set; }
        //public Vector3 PositionOnUnitSphere { get; private set; }

        public PolarCoordinates PolarCoordinates { get; private set; }

        /// <summary>
        /// Ids of the Neighbours in the same generation.
        /// Sorted clockwise.
        /// </summary>
        public string[] NeighbourIdsClockwise { get; private set; }

        private List<HexCenterPoint> _neighbours;
        /// <summary>
        /// List of all neighbours, clockwise.
        /// Note: Using of NeighbourIdsClockwise might be faster.
        /// </summary>
        public List<HexCenterPoint> Neighbours
        {
            get
            {
                if (_neighbours == null)
                {
                    Debug.Assert(NeighbourIdsClockwise != null, "That is odd. For some reason, neighbourIdsClockwise has not been assigned yet!");
                    _neighbours = new List<HexCenterPoint>();

                    foreach (string neighbourId in NeighbourIdsClockwise)
                    {
                        _neighbours.Add(HexGenerations.Instance.GetHexCenterById(neighbourId, Generation));
                    }
                }
                return _neighbours;
            }
        }

        public HexCenterPoint(string id, int generation, Vector3d positionOnUnitSphere, string[] neighbourIdsClockwise, string name = "")
        {
            Debug.Assert(positionOnUnitSphere.sqrMagnitude - 1f < 0.001, "lengths should be 1");

            Id = id;
            Generation = generation;

            //PositionOnUnitSphere = positionOnUnitSphere;
            PolarCoordinates = PolarCoordinates.UnitSphereToPolar(positionOnUnitSphere);
            //Debug.Log("PolarCoordinates = " + PolarCoordinates);
            this.NeighbourIdsClockwise = neighbourIdsClockwise;

            if (name != "")
                Name = name;
            else
            {
                Name = Id + "";
            }
        }

        public HexCenterPoint CreateMidPoint(HexCenterPoint otherCenter)
        {
            Debug.Assert(IsNeighbourTo(otherCenter), "The centers " + this.Id + " and " + otherCenter.Id + " are not neighbors!");
            //Dictionary<int, Dictionary<ulong, HexCenterPoint>> GenerationsToCheck = HexGenerations.Instance.CenterGenerations;
            //if (this.Id > otherCenter.Id)
            if (String.Compare(this.Id, otherCenter.Id, StringComparison.InvariantCultureIgnoreCase) > 0)
                {
                return otherCenter.CreateMidPoint(this);
            }
            string midpointId = GetMidpointId(this, otherCenter);
            int midpointGeneration = Generation + 1;
            Vector3d midpointPostion = GetPositionOfMidpoint(otherCenter);
            string[] neighborsOfMidpoint = GetClockwiseNeighborsOfMidpoint(otherCenter);
            return new HexCenterPoint(midpointId, midpointGeneration, midpointPostion, neighborsOfMidpoint);
            //return new HexCenterPoint(GetMidpointId(this, otherCenter), Generation + 1, GetPositionOfMidpoint(otherCenter), GetClockwiseNeighborsOfMidpoint(otherCenter));
        }

        /// <summary>
        /// Todo: this Midpoint-stuff should be part of PolarCoordinate!
        /// </summary>
        /// <param name="otherCenter"></param>
        /// <returns></returns>
        private Vector3d GetPositionOfMidpoint(HexCenterPoint otherCenter)
        {
            return (this.PolarCoordinates.PolarToUnitSphere() + otherCenter.PolarCoordinates.PolarToUnitSphere()).normalized;
        }

        private string[] GetClockwiseNeighborsOfMidpoint(HexCenterPoint otherCenter)
        {
            //Dictionary<int, Dictionary<string, HexCenterPoint>> GenerationsToCheck = HexGenerations.Instance.CenterGenerations;
            HexCenterPoint nextNeighbourOfThis = this.GetNextNeighbourClockwise(otherCenter.Id);
            HexCenterPoint nextNeighbourOfOther = otherCenter.GetNextNeighbourClockwise(this.Id);

            // asumes that this parent is the one with lower Id:
            string[] neighbours = {
            this.Id+"0",
            GetMidpointId(this, nextNeighbourOfOther),
            GetMidpointId(otherCenter, nextNeighbourOfOther),
            otherCenter.Id+"0",
            GetMidpointId(otherCenter, nextNeighbourOfThis),
            GetMidpointId(this, nextNeighbourOfThis)
        };
            return neighbours;
        }

        internal string GetNeighbourIdByNumber(int numberAsMidpoint)
        {
            return NeighbourIdsClockwise[numberAsMidpoint - 1];
        }

        private HexCenterPoint GetNextNeighbourClockwise(string id)
        {
            Debug.Assert(NeighbourIdsClockwise.Length == 5 || NeighbourIdsClockwise.Length == 6, "Wrong number of neighbourIds. This is bound to fail!");
            for (int i = 0; i < NeighbourIdsClockwise.Length; i++)
            {
                if (NeighbourIdsClockwise[i] == id)
                {
                    //ulong neighbourId = NeighbourIdsClockwise[(i + 1) % NeighbourIdsClockwise.Length];
                    string neighbourId = NeighbourIdsClockwise[(i + 1) % NeighbourIdsClockwise.Length];
                    return HexGenerations.Instance.GetHexCenterById(neighbourId, this.Generation);
                }
            }
            throw new Exception("the id " + id + " seems not to be a neighbour of " + this.Id + "!");
        }



        private int GetPositionInClockwiseNeighbours(HexCenterPoint otherCenter)
        {
            Debug.Assert(NeighbourIdsClockwise != null, "That is odd. For some reason, neighbourIdsClockwise has not been assigned yet!");
            for (int i = 0; i < NeighbourIdsClockwise.Length; i++)
            {
                if (otherCenter.Id == NeighbourIdsClockwise[i])
                {
                    return i;
                }
            }
            Debug.LogError("The center " + otherCenter.Id + " is not a neighbour to " + this.Id + 
                "\n The neighbours are:" +
                NeighbourIdsClockwise[0] + ", "+
                NeighbourIdsClockwise[1] + ", "+
                NeighbourIdsClockwise[2] + ", "+
                NeighbourIdsClockwise[3] + ", "+
                NeighbourIdsClockwise[4] + ", "+
                NeighbourIdsClockwise[5] + ". "
                );
            return 0;
        }

        /// <summary>
        /// This can be used for debugging. 
        /// </summary>
        public bool IsNeighbourTo(HexCenterPoint otherCenter)
        {
            Debug.Assert(NeighbourIdsClockwise != null, "That is odd. For some reason, neighbourIdsClockwise has not been assigned yet!");
            for (int i = 0; i < NeighbourIdsClockwise.Length; i++)
            {
                if (otherCenter.Id == NeighbourIdsClockwise[i])
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// This can be used for debugging. 
        /// </summary>
        public string StringOfAllNeighbours()
        {
            Debug.Assert(NeighbourIdsClockwise != null, "That is odd. For some reason, neighbourIdsClockwise has not been assigned yet!");
            string result = NeighbourIdsClockwise[0] +"";
            for (int i = 1; i < NeighbourIdsClockwise.Length; i++)
            {
                result += ", " + NeighbourIdsClockwise[i]; 
                
            }
            return result;
        }

        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="forceCreation">This should only be set to true when called from HexGenerations</param>
        /// <returns></returns>
        internal HexCenterPoint CloneForNextGeneration(bool forceCreation = false)
        {
            string newId = this.Id + "0";
            if (!forceCreation)
            {
                return HexGenerations.Instance.GetHexCenterById(newId, Generation + 1);
            }
            string[] newNeighbourIdsClockwise = new string[NeighbourIdsClockwise.Length];
            for (int i = 0; i < NeighbourIdsClockwise.Length; i++)
            {
                HexCenterPoint neighbour = HexGenerations.Instance.GetHexCenterById(NeighbourIdsClockwise[i], Generation);
                Debug.Assert(neighbour.Id == NeighbourIdsClockwise[i], "GetHexCenterById(" + NeighbourIdsClockwise[i] + "," + Generation + ") returned a center with the id " + neighbour.Id + "!");
                if (this.Id == neighbour.Id)
                {
                    Dictionary<int, Dictionary<string, HexCenterPoint>> GenerationsToCheck = HexGenerations.Instance.CenterGenerations;
                    Debug.LogError("hey, this is not a neighbour!");
                }
                newNeighbourIdsClockwise[i] = GetMidpointId(this, neighbour);
            }
            HexCenterPoint result = new HexCenterPoint(newId, this.Generation + 1, this.PolarCoordinates.PolarToUnitSphere(), newNeighbourIdsClockwise, this.Name + "I");
            return result;
        }

    }
}
