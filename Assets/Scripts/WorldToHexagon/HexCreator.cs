﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace WorldToHexagons
{
    public class HexCreator
    {
        
        private Dictionary<string, HexCenterPoint> usedHexes = new Dictionary<string, HexCenterPoint>();



        /// <summary>
        /// Generates basic information about Hexes, not any Hex-Geometry.
        /// </summary>
        /// <returns>Only new hexes that needed to be created.</returns>
        public List<HexCenterPoint> CreateNecessaryHexCenters(PolarCoordinates areaCenter, uint maxGenerations)
        {
            HexCenterPoint nearestCenter;
            List<HexCenterPoint> nearestCentersMany;
            int generation;

            nearestCentersMany = new List<HexCenterPoint>();
            foreach(KeyValuePair<string,HexCenterPoint> keyValuePair in HexGenerations.Instance.CenterGenerations[0])
            {
                nearestCentersMany.Add(keyValuePair.Value);
            }
            nearestCenter = NearestCenter(nearestCentersMany, areaCenter);
            generation = 1;

            while (generation <= maxGenerations-1)
            {
                nearestCentersMany = CreateDoubleRingOfChildren(nearestCenter, generation);

                nearestCenter = NearestCenter(nearestCentersMany, areaCenter);


                generation++;
            }

            // The last (few) iterations create a "buffer" of hexes around the user. not only the three nearest hexes:
            //while (generation <= maxGenerations )
            //{
            //   nearestCentersMany = CreateNewGenerationAroundCenters(nearestCentersMany, generation);
            //    generation++;
            //}

            //nearestCentersMany = CreateDoubleRingOfChildren(nearestCenter, generation);
            nearestCentersMany = CreateNewGenerationAroundCenters(nearestCentersMany, generation);
            //nearestCentersMany = CreateNewGenerationAroundCenters(nearestCentersMany, generation+1);
            List<HexCenterPoint> newHexes = new List<HexCenterPoint>();
            foreach (HexCenterPoint hex in nearestCentersMany)
            {
                if (!usedHexes.ContainsKey(hex.Id))
                {
                    newHexes.Add(hex);
                    usedHexes.Add(hex.Id, hex);
                }

            }

            //newHexes.Add(nearestCenter);
            //usedHexes.Add(nearestCenter.Id, nearestCenter);



            string debugString = "Finished creating hexes. Hexes overall: " + usedHexes.Count + ". New Hexes: " + newHexes.Count + "\n usedHexes: ";
            foreach (KeyValuePair<string, HexCenterPoint> keyvalue in usedHexes)
            {
                debugString += keyvalue.Key + ", ";
            }
            debugString += "\n newHexes: ";
            foreach (HexCenterPoint hex in newHexes)
            {
                debugString += hex.Id + ", ";
            }

            Debug.Log(debugString);
            //return nearestCentersMany;

            return newHexes;
        }

        private HexCenterPoint NearestCenter(List<HexCenterPoint> centers, PolarCoordinates userPosition)
        {
            //Debug.Assert(userPositionOnUnitSphere.magnitude == 1, "The userPosition on UnitSphere has a length of " + userPositionOnUnitSphere.magnitude);
            HexCenterPoint nearest = null;
            double nearestDistance = double.MaxValue;


            foreach (HexCenterPoint center in centers)
            {
                double distance = PolarCoordinates.DistanceAngleEuler(userPosition, center.PolarCoordinates);
                if (distance < nearestDistance)
                {
                    nearest = center;
                    nearestDistance = distance;
                }
            }
            Debug.Assert(nearest != null);
            Debug.Assert(MinDistanceAngleInList(userPosition, centers) == PolarCoordinates.DistanceAngleEuler(userPosition, nearest.PolarCoordinates), "Calculation of the nearest center failed");

            return nearest;
        }

        private List<HexCenterPoint> CreateDoubleRingOfChildren(HexCenterPoint originalCenter, int generation)
        {
            List<HexCenterPoint> originalCenters = new List<HexCenterPoint>();
            originalCenters.Add(originalCenter);
            foreach(HexCenterPoint neighbor in originalCenter.Neighbours)
            {
                originalCenters.Add(neighbor);
            }

            AssertListOfUniques(originalCenters);
            List<HexCenterPoint> result = new List<HexCenterPoint>();
            foreach (HexCenterPoint center in originalCenters)
            {
                result.Add(center.CloneForNextGeneration());
                foreach (HexCenterPoint potentialNeighbor in originalCenters)
                {
                    if (!center.NeighbourIdsClockwise.Contains(potentialNeighbor.Id))
                        continue;
                    string midpointId = HexCenterPoint.GetMidpointId(center, potentialNeighbor);
                    if (result.Any(c => c.Id == midpointId))
                        continue;
                    HexCenterPoint midpoint = center.CreateMidPoint(potentialNeighbor);

                    result.Add(midpoint);
                }
            }

            AssertListOfUniques(result);

            return result;
        }


        private List<HexCenterPoint> CreateNewGenerationAroundCenters(List<HexCenterPoint> parentCenters, int generation)
        {
            //Dictionary<int, Dictionary<string, HexCenterPoint>> GenerationsToCheck = HexGenerations.Instance.CenterGenerations;
            // 1. Make sure the Dictionary for the (new?) generation exists.
            if (!HexGenerations.Instance.CenterGenerations.ContainsKey(generation))
            {
                HexGenerations.Instance.CenterGenerations.Add(generation, new Dictionary<string, HexCenterPoint>());
            }


            List<HexCenterPoint> result = new List<HexCenterPoint>();

            foreach (HexCenterPoint center in parentCenters)
            {
                HexCenterPoint directChild = center.CloneForNextGeneration();
                result.Add(directChild);
                foreach (string neighborId in directChild.NeighbourIdsClockwise)
                {
                    HexCenterPoint neighbour = HexGenerations.Instance.GetHexCenterById(neighborId, generation);

                    if (!result.Contains(neighbour))
                    {
                        result.Add(neighbour);
                    }
                }
            }
            return result;

        }

#region Validations and Asserts:
        private bool AssertListOfUniques(List<HexCenterPoint> centers)
        {

            for (int i = 0; i < centers.Count; i++)
            {

                for (int j = 0; j < centers.Count; j++)
                {
                    if (centers[i].Id == centers[j].Id && i != j)
                    {
                        string debugString = "List: ";
                        foreach (HexCenterPoint center in centers)
                        {
                            debugString += center.Id + ", ";
                        }
                        Debug.LogError("The following List contains the Hex-Id " + centers[j].Id + " more than once: " + debugString + " (i=" + i + ",j=" + j + ")");
                        return false;


                    }
                }
            }
            return true;
        }

        private double MinDistanceAngleInList(PolarCoordinates point, List<HexCenterPoint> centers)
        {
            double min = double.MaxValue;
            foreach (HexCenterPoint center in centers)
            {
                min = Mathd.Min(min, PolarCoordinates.DistanceAngleEuler(point, center.PolarCoordinates));
            }
            return min;
        }
        #endregion

    }
}