﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace WorldToHexagons
{
    internal class EarthIcosaeder  {

        private Dictionary<string, HexCenterPoint> savedIcosaeder;

        public Dictionary<string, HexCenterPoint> SavedIcosaeder
        {
            get
            {
                if (savedIcosaeder == null)
                    savedIcosaeder = GenerateIcosaeder();
                return savedIcosaeder;
            }
        }
        private Dictionary<string, HexCenterPoint> GenerateIcosaeder()
        {
            Dictionary<string, HexCenterPoint> centers = new Dictionary<string, HexCenterPoint>();
            centers["10"] = new HexCenterPoint("10", 0, new Vector3d(0, 1, 0),  new string[] { "11", "12", "13", "14", "15" },"NorthPole");
            centers["20"] = new HexCenterPoint("20", 0, new Vector3d(0, -1, 0), new string[] { "21", "22", "23", "24", "25" },"SouthPole");

            // Children of NorthPole, clockwise:
            centers["11"] = new HexCenterPoint("11", 0, new Vector3d( 0,           0.44721365f,  0.89442718f), new string[] { "10", "15", "21", "25", "12" }, "Sahara");
            centers["12"] = new HexCenterPoint("12", 0, new Vector3d( +0.85065079f, 0.44721365f,  0.27639326f), new string[] { "10", "11", "25", "24", "13" }, "BermudaTriangle");
            centers["13"] = new HexCenterPoint("13", 0, new Vector3d( +0.52573109f, 0.44721359f, -0.72360688f), new string[] { "10", "12", "24", "23", "14" }, "NorthPacific");
            centers["14"] = new HexCenterPoint("14", 0, new Vector3d(-0.52573109f, 0.44721362f, -0.72360688f), new string[] { "10", "13", "23", "22", "15" }, "JapanesePacific");// Matsukaze Shipwreck
            centers["15"] = new HexCenterPoint("15", 0, new Vector3d(-0.85065079f, 0.44721362f,  0.27639338f), new string[] { "10", "14", "22", "21", "11" }, "Rajasthan");
        
            // Children of SouthPole, clockwise:
            centers["21"] = new HexCenterPoint("21", 0, new Vector3d(-0.52573121f, -0.44721344f,  0.72360671f), new string[] { "20", "25", "11", "15", "22" }, "MozambiqueBasin");
            centers["22"] = new HexCenterPoint("22", 0, new Vector3d(-0.85065073f, -0.44721341f, -0.27639323f), new string[] { "20", "21", "15", "14", "23" },"PerthBasin");
            centers["23"] = new HexCenterPoint("23", 0, new Vector3d( 0f,          -0.44721353f, -0.89442724f), new string[] { "20", "22", "14", "13", "24" },"Polynesia");
            centers["24"] = new HexCenterPoint("24", 0, new Vector3d( +0.85065091f, -0.44721365f, -0.27639312f), new string[] { "20", "23", "13", "12", "25" },"EasterIsland");
            centers["25"] = new HexCenterPoint("25", 0, new Vector3d( +0.52573115f, -0.44721365f,  0.72360694f), new string[] { "20", "24", "12", "11", "21" },"BrasilianCoast");
            return centers;
        }

        private List<HexCenterPoint> centerList;
        public List<HexCenterPoint> CenterList
        {
            get
            {
                //Debug.LogWarning("Someone is getting the icosaeder as a list - which is not a good idea. I think.");
                if (centerList == null) 
                {
                    centerList = GenerateIcosaederAsCenterList();
                }
                return centerList;
            }
            
        }

        private List<HexCenterPoint> GenerateIcosaederAsCenterList()
        {
            Debug.Assert(SavedIcosaeder.Count == 12, "There were not 12 points in the given Icosaeder, but " + SavedIcosaeder.Count + "!");

            List<HexCenterPoint> result = new List<HexCenterPoint>();
            foreach (KeyValuePair<string, HexCenterPoint> KeyCenterPair in SavedIcosaeder)
            {
                result.Add(KeyCenterPair.Value);
            }
            return result;
        }
    }
}

