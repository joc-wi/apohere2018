﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace WorldToHexagons
{
    public class HexGenerations : SingletonBase<HexGenerations> {

        //private Dictionary<int,HexCenterPoint> _icosaederDoNotFuckingTouchThisPrivateVariableEvenInsideTheClass;

        public Dictionary<string, HexCenterPoint> IcosaederPoints{get{return CenterGenerations[0];}}

        private Dictionary<int, Dictionary<string, HexCenterPoint>> _centerGenerationsDoNotFuckingTouchThisPrivateVariableEvenInsideTheClass;
        EarthIcosaeder earthIcosaeder = new EarthIcosaeder();
        public Dictionary<int, Dictionary<string, HexCenterPoint>> CenterGenerations
        {
            get
            {
                if (_centerGenerationsDoNotFuckingTouchThisPrivateVariableEvenInsideTheClass == null)
                {
                    _centerGenerationsDoNotFuckingTouchThisPrivateVariableEvenInsideTheClass = new Dictionary<int, Dictionary<string, HexCenterPoint>>();
                    _centerGenerationsDoNotFuckingTouchThisPrivateVariableEvenInsideTheClass[0] = earthIcosaeder.SavedIcosaeder;

                }
                return _centerGenerationsDoNotFuckingTouchThisPrivateVariableEvenInsideTheClass;
            }

        }
        
        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HexCenterPoint GetHexCenterById(string id, int generation)
        {
            
            HexCenterPoint result;
            // Create Generation if necessary:
            if(CenterGenerations.ContainsKey(generation) == false)
            {
                Debug.Assert(generation >= 0);
                CenterGenerations[generation] = new Dictionary<string, HexCenterPoint>();
            }

            // if Center already exists, return it:
            if (CenterGenerations[generation].ContainsKey(id))
            {
                return CenterGenerations[generation][id];
            }
            // if Center does not exist...
            else
            {
                //Debug.Log("Generation " + generation + " did not contain the Key " + id + "...");
                Debug.Assert(generation != 0, "Did not find id " + id + " in generation 0. But Generation 0 should be predefined and complete! That's odd.");
                int parentGeneration = generation - 1;
                //ulong fatherId = (ulong)(id /10); // Note: /10 might not be the fastest. but *0.1f drags it down to float-exactness, which is kind of a problem here.
                string fatherId = id.Remove(id.Length - 1); ; // Note: /10 might not be the fastest. but *0.1f drags it down to float-exactness, which is kind of a problem here.

                //int NumberAsMidpoint = (int)(id - fatherId * 10);
                int NumberAsMidpoint = int.Parse(id.Substring(id.Length-1));
                //Debug.Log("id is " + id + ", so fatherId is " + fatherId + " and NumberAsMidpoint is " + NumberAsMidpoint);

              
                HexCenterPoint father = GetHexCenterById(fatherId, parentGeneration);

                if(NumberAsMidpoint == 0)
                {
                    
                    result = father.CloneForNextGeneration(true);
                    CenterGenerations[generation][id] = result;
                    return result;
                }
                else
                {
                    string motherId = father.GetNeighbourIdByNumber(NumberAsMidpoint);
                    //Debug.Log("motherId should be " + motherId);
                    //Debug.Assert(motherId > fatherId, "The motherId is not bigger than the fatherId. something went wrong.");
                    Debug.Assert(String.Compare(motherId,fatherId, StringComparison.InvariantCultureIgnoreCase)>0, "The motherId is not bigger than the fatherId. something went wrong.");
                    HexCenterPoint mother = GetHexCenterById(motherId, parentGeneration);
                    result = father.CreateMidPoint(mother);
                    CenterGenerations[generation][id] = result;
                    return result;
                } 
            } 
        }


        private void AddToListIfListExists(List<HexCenterPoint>newHexes, HexCenterPoint hex) {
            if (newHexes != null)
                newHexes.Add(hex);
        }
        
    }
}
