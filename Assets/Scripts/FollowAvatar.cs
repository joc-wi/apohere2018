﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowAvatar : MonoBehaviour {

    [SerializeField]
    private Transform Avatar;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Avatar.position != this.transform.position)
        {
            this.transform.position = Avatar.position;
        }
	}
}
