﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomTools //Todo: somehow make this an extension or overwrite of UnityEngine.Random
{

    public static Color RandomColorVariation(Color originalColor, float maxVariation = 0.1f)
    {
        float variation = maxVariation * 0.5f;
        float greenVariation = maxVariation;
        return new Color(
            Mathf.Min(Mathf.Max(originalColor.r + Random.Range(-variation, variation), 0), 1),
            Mathf.Min(Mathf.Max(originalColor.g + Random.Range(-greenVariation, greenVariation), 0), 1),
            Mathf.Min(Mathf.Max(originalColor.b + Random.Range(-variation, variation), 0), 1),
            originalColor.a
            );
    }

    /// <summary>
    /// A random vector. This is fast, but can end up longer than radius.
    /// </summary>
    /// <param name="radius"></param>
    /// <returns></returns>
    public static Vector3 VectorSimple(float radius, bool yToZero = false)
    {
        return new Vector3(Random.Range(-radius, radius), yToZero ? 0: Random.Range(-radius, radius), Random.Range(-radius, radius));
    }

    
}
