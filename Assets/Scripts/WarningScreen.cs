﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarningScreen : MonoBehaviour {
    [SerializeField] GameObject warningScreenCanvas;
	// Use this for initialization
	void Awake () {
        RequestRailroad.LoadingFailedEvent += LoadingFailedEventHandler;
        warningScreenCanvas.gameObject.SetActive(false);
    }
	private void LoadingFailedEventHandler()
    {
        warningScreenCanvas.gameObject.SetActive(true);
    }
	
}
