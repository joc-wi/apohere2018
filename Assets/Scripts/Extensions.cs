﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions  {


    /// <summary>
    /// Todo: why does this need a "this" in front of it when used?
    /// </summary>
    /// <param name="monoBehaviour"></param>
    /// <param name="value"></param>
	public static void AssertExistence(this MonoBehaviour monoBehaviour, object value)
    {
        Debug.Assert(value != null);
    }
}
