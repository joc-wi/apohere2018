﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class SingletonBase<T> where T : SingletonBase<T>, new()
{
    static private T instance;
    static public T Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new T();
            }
            return instance;
        }
    }

    


}
