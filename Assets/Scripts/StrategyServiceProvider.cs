﻿using JetBrains.Annotations;
using ProjectionStrategies;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrategyServiceProvider : MonoSingleton<StrategyServiceProvider> {

    //[SerializeField]
    //[UsedImplicitly]
    //private ProvideMapStringBase mapStringProvider;
    //public ProvideMapStringBase MapStringProvider { get { return mapStringProvider; } }
    [SerializeField]
    [UsedImplicitly]
    private IProjectionStrategy projector = new EllipticalMercator();
    public IProjectionStrategy Projector { get { return projector; } }

    void Start()
    {
        //Debug.Assert(mapStringProvider != null);

    }
}
