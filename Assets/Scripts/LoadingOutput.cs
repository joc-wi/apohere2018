﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LoadingOutput : MonoBehaviour {
    public static LoadingOutput Instance;
    private Text textField;
	// Use this for initialization
	void OnEnable () {
        Instance = this;
        textField = GetComponent<Text>();
	}
	
	public static void Log(string message)
    {
        if(Instance == null)
        {
            Debug.LogWarning("No Insance of LoadingOutput! (wanted to print '" + message + "')");
            return;
        }
        Instance.textField.text += message + "\n";
    }
}
