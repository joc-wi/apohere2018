﻿using Deserialisation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;

namespace GeometyCreation
{
    public class Road : MonoBehaviour
    {
        [SerializeField] private Material roadMaterial;
        [SerializeField] private float roadHeight = 0.05f;
        [SerializeField] private GameObject[] carPrefabs;
        [SerializeField] private StreetNameIndicator streetNamePrefab;
        private float roadWidth;

        private string streetName;

        private void Start()
        {
            Debug.Assert(roadMaterial != null);
            Debug.Assert(carPrefabs != null);
            Debug.Assert(streetNamePrefab != null);
        }
        public void Init(OsmBounds currentBounds, OsmWay way)
        {
            ExtractInformation(way);
            CreateRoadGeometry(currentBounds, way);
            CreateCars(currentBounds, way);
            CreateNameIndicator(currentBounds, way);
        }
        private void ExtractInformation(OsmWay way)
        {
            if (way.Tags.ContainsKey("highway"))
            {
                breakDownHighwayTag(way.Tags["highway"]);
            }

            if (way.Tags.ContainsKey("name"))
            {
                //Debug.Log("Streetname: " + way.Tags["name"]);
                streetName = way.Tags["name"];
            }


        }
        private void breakDownHighwayTag(string value)
        {
            /// See here: https://taginfo.openstreetmap.org/keys/highway#values
            switch (value)
            {
                case "primary":
                    roadWidth = 5;//Todo: check this.
                    break;
                case "tertiary":
                    roadWidth = 5;//Todo: check this.
                    break;
                case "living_street":
                    roadWidth = 5;//Todo: check this.
                    break;
                case "residential":
                    roadWidth = 5;
                    break;
                case "service":
                    roadWidth = 4;
                    break;
                case "track":
                    roadWidth = 5;
                    break;
                case "unclassified":
                    roadWidth = 4;
                    break;
                case "pedestrian":
                    roadWidth = 4;
                    break;
                case "path":
                    roadWidth = 3;
                    break;
                case "footway":
                    roadWidth = 1.5f;
                    break;
                case "steps":
                    roadWidth = 3f;
                    break;
                default:
                    roadWidth = 4;
                    Debug.LogWarning("I did not recognise highway type " + value + ".");
                    break;
            }
        }
        private void CreateRoadGeometry(OsmBounds currentBounds, OsmWay way)
        {
            //Debug.Log("starting to createRoadGeometry");
            GameObject roadGeometry = new GameObject("Highway_f" + way.Id);
            roadGeometry.transform.SetParent(this.transform);
            //Vector3 buildingOriginLocal = GetCenterGlobal(way) - CenterOfUnitySpace.CenterMapSpace;

            roadGeometry.transform.position = way.WayCenter.UnitySpace;
            MeshFilter meshFilter = roadGeometry.AddComponent<MeshFilter>();
            /*MeshRenderer meshRender =*/
            roadGeometry.AddComponent<MeshRenderer>();
            roadGeometry.GetComponent<Renderer>().material = roadMaterial;


            List<Vector3> vectors = new List<Vector3>();
            List<Vector3> normals = new List<Vector3>();
            List<int> indices = new List<int>();
            List<Vector2> uvs = new List<Vector2>();

            Vector3 faceNormal;

            OsmNode node0 = way.Nodes[0];
            OsmNode node1 = way.Nodes[0];
            OsmNode node2 = way.Nodes[0];
            OsmNode node3 = way.Nodes[1];

            for (int i = 1; i < way.Nodes.Count; i++)
            {
                node0 = node1;
                node1 = node2;
                node2 = node3;
                node3 = way.Nodes[Mathf.Min(i + 1, way.Nodes.Count - 1)];

                Vector3 sideVector1 = Vector3.Cross(node0.GlobalPosition - node1.GlobalPosition, Vector3.up) +
                                        Vector3.Cross(node1.GlobalPosition - node2.GlobalPosition, Vector3.up);

                Vector3 sideVector2 = Vector3.Cross(node1.GlobalPosition - node2.GlobalPosition, Vector3.up) +
                                        Vector3.Cross(node2.GlobalPosition - node3.GlobalPosition, Vector3.up);
                sideVector1 = sideVector1.normalized * roadWidth * 0.5f;
                sideVector2 = sideVector2.normalized * roadWidth * 0.5f;

                Vector3 point1right = node1.PolarCoordinates.UnitySpace - way.WayCenter.UnitySpace + 5 * sideVector1 + Vector3.up * roadHeight;
                Vector3 point1left = node1.PolarCoordinates.UnitySpace - way.WayCenter.UnitySpace - 5 * sideVector1 + Vector3.up * roadHeight;
                Vector3 point2right = node2.PolarCoordinates.UnitySpace - way.WayCenter.UnitySpace + 5 * sideVector2 + Vector3.up * roadHeight;
                Vector3 point2left = node2.PolarCoordinates.UnitySpace - way.WayCenter.UnitySpace - 5 * sideVector2 + Vector3.up * roadHeight;

                //Debug.Log("my Points are " + point1right + ", " + point1left + ", " + point2right + " and " + point2left);

                faceNormal = Vector3.up;
                normals.Add(faceNormal);
                normals.Add(faceNormal);
                normals.Add(faceNormal);
                normals.Add(faceNormal);

                vectors.Add(point1right);
                vectors.Add(point1left);
                vectors.Add(point2right);
                vectors.Add(point2left);

                int index1, index2, index3, index4;
                index4 = vectors.Count - 1;
                index3 = vectors.Count - 2;
                index2 = vectors.Count - 3;
                index1 = vectors.Count - 4;

                // first triangle:
                indices.Add(index2);
                indices.Add(index3);
                indices.Add(index1);

                // second triangle:
                indices.Add(index2);
                indices.Add(index4);
                indices.Add(index3);

                uvs.Add(new Vector2(0, 0));
                uvs.Add(new Vector2(1, 0));
                uvs.Add(new Vector2(0, 1));
                uvs.Add(new Vector2(1, 1));

            }

            meshFilter.mesh.vertices = vectors.ToArray();
            meshFilter.mesh.normals = normals.ToArray();
            meshFilter.mesh.triangles = indices.ToArray();
            meshFilter.mesh.uv = uvs.ToArray();
        }
        private void CreateCars(OsmBounds currentBounds, OsmWay way)
        {
            //float minCarDistance = 3f;
            //float maxCarDistance = 15f;
            
            
            for(int i = 1; i< way.Nodes.Count; i++)
            {
                OsmNode nodeA = way.Nodes[i - 1];
                OsmNode nodeB = way.Nodes[i];

                Vector3 position = 0.5f * (nodeA.PolarCoordinates.UnitySpace + nodeB.PolarCoordinates.UnitySpace);
                RandomCar(position+ RandomTools.VectorSimple(4f, true));
            }
        }
        private GameObject RandomCar(Vector3 position)
        {
            GameObject car = Instantiate(carPrefabs[UnityEngine.Random.Range(0, carPrefabs.Length)]);
            car.transform.SetParent(this.transform);
            car.transform.position = position;
            car.transform.rotation = Quaternion.Euler(0, UnityEngine.Random.Range(0, 360),0);
            return car;
        }

        private void CreateNameIndicator(OsmBounds currentBounds, OsmWay way)
        {
            if (!way.Tags.ContainsKey("name"))
            {
                return;
            }
            if (way.Nodes.Count < 3)
            {
                return;
            }

            //int repetitionsOfName = (int)way.Length / 100;

            //for(int i = 0; i < repetitionsOfName; i++)
            //{

            //}
            OsmNode middleNode = way.Nodes[way.Nodes.Count / 2];
            OsmNode oneBeforeMiddleNode = way.Nodes[(way.Nodes.Count / 2)-1];
            Vector3 roadDirection = middleNode.PolarCoordinates.UnitySpace - oneBeforeMiddleNode.PolarCoordinates.UnitySpace;
            float angle = Mathf.Rad2Deg * Mathf.Atan(-roadDirection.z / roadDirection.x);


            StreetNameIndicator nameIndicator = Instantiate(streetNamePrefab);
            nameIndicator.transform.SetParent(this.transform);
            nameIndicator.transform.position = middleNode.PolarCoordinates.UnitySpace + Vector3.up * 2;
            nameIndicator.transform.rotation = Quaternion.Euler(90, angle, 0);
            nameIndicator.StreetName = way.Tags["name"];
        }
    }
}