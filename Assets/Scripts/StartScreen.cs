﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StartScreen : MonoBehaviour {

    //[SerializeField]private PolarCoordinates areaCenterDefault = new PolarCoordinates(47.887f, 7.722f);

    [SerializeField] private Button startButton;

    [SerializeField] private InputField latitudeInput;
    [SerializeField] private InputField longitudeInput;

    [SerializeField] private Dropdown countryDropdown;
    [SerializeField] private Dropdown cityDropdown;


    [SerializeField] private GameObject loadingScreen;


    Dictionary<string, PolarCoordinates> cityCoordinatesDict = new Dictionary<string, PolarCoordinates>
    {
        { "Australia, Perth", new PolarCoordinates(-31.89823, 115.88078) },
        { "Germany, Staufen", new PolarCoordinates(47.887, 7.722) },
        { "Germany, Bamberg", new PolarCoordinates(49.891499, 10.887500) },
        { "Great Britain, Nottingham", new PolarCoordinates(52.95384, -1.15673) },
        { "India, Bengaluru", new PolarCoordinates(12.925112, 77.618078) },
        { "Spain, Barcelona", new PolarCoordinates(41.39782, 2.12896) }
    }; 
    

    // Use this for initialization
    void Start () {
        this.AssertExistence(startButton);
        this.AssertExistence(loadingScreen);
        this.AssertExistence(latitudeInput);
        this.AssertExistence(longitudeInput);
        this.AssertExistence(countryDropdown);
        this.AssertExistence(cityDropdown);

        //countryDropdown.ClearOptions();
        //countryDropdown.AddOptions(CountriesAsOptions());

        cityDropdown.ClearOptions();
        cityDropdown.AddOptions(CitiesAsOptions());
        cityDropdown.onValueChanged.AddListener(CityDropdownValueChangedHandler);

        // trigger this once, so that the lat/lon fields allign with the value in the dropdown:
        CityDropdownValueChangedHandler(0);
        
        latitudeInput.onEndEdit.AddListener(LatitudeEditEventHandler);
        longitudeInput.onEndEdit.AddListener(LongitudeEditEventHandler);
        startButton.onClick.AddListener(StartGame);


	}

    private void CityDropdownValueChangedHandler(int arg0)
    {
        string city = cityDropdown.captionText.text;
        PolarCoordinates coordinates = cityCoordinatesDict[city];
        latitudeInput.text = coordinates.lat.ToString(System.Globalization.CultureInfo.InvariantCulture);
        longitudeInput.text = coordinates.lon.ToString(System.Globalization.CultureInfo.InvariantCulture);
    }

    private void LongitudeEditEventHandler(string textOfField)
    {
        double lon = double.Parse(textOfField);
        if (PolarCoordinates.CanBeUsedAsLongitude(lon))
        {
            latitudeInput.image.color = Color.white;
        }
        else
        {
            latitudeInput.image.color = Color.red;
        }
    }

    private void LatitudeEditEventHandler(string textOfField)
    {
        double lat = double.Parse(textOfField);
        if (PolarCoordinates.CanBeUsedAsLatitude(lat))
        {
            latitudeInput.image.color = Color.white;
        }
        else
        {
            latitudeInput.image.color = Color.red;
        }
    }

    private void StartGame()
    {
        double lat = double.Parse(latitudeInput.text);
        double lon = double.Parse(longitudeInput.text);

        if (!PolarCoordinates.CanBeUsedAsPolarCoordinates(lat,lon))
        {
            return;
        }

        PolarCoordinates areaCenter = new PolarCoordinates(lat, lon);
        FindObjectOfType<RequestRailroad>().RequestNewMapIfNecessary(areaCenter);
        this.gameObject.SetActive(false);
        loadingScreen.gameObject.SetActive(true);
    }

    private List<Dropdown.OptionData> CountriesAsOptions()
    {
        List<Dropdown.OptionData> result = new List<Dropdown.OptionData>();
        foreach(string country in countries)
        {
            result.Add(new Dropdown.OptionData(country));
        }
        return result;
    }

    private List<Dropdown.OptionData> CitiesAsOptions()
    {
        List<Dropdown.OptionData> result = new List<Dropdown.OptionData>();
        foreach (KeyValuePair<string, PolarCoordinates> city in cityCoordinatesDict)
        {
            result.Add(new Dropdown.OptionData(city.Key));
        }
        return result;
    }
    private string[] countries =
    {
        "Afghanistan",
        "Albania",
        "Algeria",
        "Andorra",
        "Angola",
        "Antigua and Barbuda",
        "Argentina",
        "Armenia",
        "Australia",
        "Austria",
        "Azerbaijan",
        "Bahamas",
        "Bahrain",
        "Bangladesh",
        "Barbados",
        "Belarus",
        "Belgium",
        "Belize",
        "Benin",
        "Bhutan",
        "Bolivia",
        "Bosnia and Herzegovina",
        "Botswana",
        "Brazil",
        "Brunei",
        "Bulgaria",
        "Burkina Faso",
        "Burundi",
        "Cabo Verde",
        "Cambodia",
        "Cameroon",
        "Canada",
        "Central African Republic (CAR)",
        "Chad",
        "Chile",
        "China",
        "Colombia",
        "Comoros",
        "Democratic Republic of the Congo",
        "Republic of the Congo",
        "Costa Rica",
        "Cote d'Ivoire",
        "Croatia",
        "Cuba",
        "Cyprus",
        "Czech Republic",
        "Denmark",
        "Djibouti",
        "Dominica",
        "Dominican Republic",
        "Ecuador",
        "Egypt",
        "El Salvador",
        "Equatorial Guinea",
        "Eritrea",
        "Estonia",
        "Eswatini (formerly Swaziland)",
        "Ethiopia",
        "Fiji",
        "Finland",
        "France",
        "Gabon",
        "Gambia",
        "Georgia",
        "Germany",
        "Ghana",
        "Greece",
        "Grenada",
        "Guatemala",
        "Guinea",
        "Guinea-Bissau",
        "Guyana",
        "Haiti",
        "Honduras",
        "Hungary",
        "Iceland",
        "India",
        "Indonesia",
        "Iran",
        "Iraq",
        "Ireland",
        "Israel",
        "Italy",
        "Jamaica",
        "Japan",
        "Jordan",
        "Kazakhstan",
        "Kenya",
        "Kiribati",
        "Kosovo",
        "Kuwait",
        "Kyrgyzstan",
        "Laos",
        "Latvia",
        "Lebanon",
        "Lesotho",
        "Liberia",
        "Libya",
        "Liechtenstein",
        "Lithuania",
        "Luxembourg",
        "Macedonia (FYROM)",
        "Madagascar",
        "Malawi",
        "Malaysia",
        "Maldives",
        "Mali",
        "Malta",
        "Marshall Islands",
        "Mauritania",
        "Mauritius",
        "Mexico",
        "Micronesia",
        "Moldova",
        "Monaco",
        "Mongolia",
        "Montenegro",
        "Morocco",
        "Mozambique",
        "Myanmar (formerly Burma)",
        "Namibia",
        "Nauru",
        "Nepal",
        "Netherlands",
        "New Zealand",
        "Nicaragua",
        "Niger",
        "Nigeria",
        "North Korea",
        "Norway",
        "Oman",
        "Pakistan",
        "Palau",
        "Palestine",
        "Panama",
        "Papua New Guinea",
        "Paraguay",
        "Peru",
        "Philippines",
        "Poland",
        "Portugal",
        "Qatar",
        "Romania",
        "Russia",
        "Rwanda",
        "Saint Kitts and Nevis",
        "Saint Lucia",
        "Saint Vincent and the Grenadines",
        "Samoa",
        "San Marino",
        "Sao Tome and Principe",
        "Saudi Arabia",
        "Senegal",
        "Serbia",
        "Seychelles",
        "Sierra Leone",
        "Singapore",
        "Slovakia",
        "Slovenia",
        "Solomon Islands",
        "Somalia",
        "South Africa",
        "South Korea",
        "South Sudan",
        "Spain",
        "Sri Lanka",
        "Sudan",
        "Suriname",
        "Swaziland (renamed to Eswatini)",
        "Sweden",
        "Switzerland",
        "Syria",
        "Taiwan",
        "Tajikistan",
        "Tanzania",
        "Thailand",
        "Timor-Leste",
        "Togo",
        "Tonga",
        "Trinidad and Tobago",
        "Tunisia",
        "Turkey",
        "Turkmenistan",
        "Tuvalu",
        "Uganda",
        "Ukraine",
        "United Arab Emirates (UAE)",
        "United Kingdom (UK)",
        "United States of America (USA)",
        "Uruguay",
        "Uzbekistan",
        "Vanuatu",
        "Vatican City (Holy See)",
        "Venezuela",
        "Vietnam",
        "Yemen",
        "Zambia",
        "Zimbabwe"
    };

}


