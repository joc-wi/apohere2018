﻿using Deserialisation;
using ProjectionStrategies;
using System;
using UnityEngine;

[Serializable]
public class PolarCoordinates
{
    private const double deg2Rad = 0.01745329251994329576923690768489;
    private const double rad2Deg = 57.295779513082320876798154814105;
    private const double EarthRadius_Average = 6370000.0;
    static private IProjectionStrategy __projector;
    static private IProjectionStrategy Projector
    {
        get
        {
            if( __projector == null)
            {
                __projector = StrategyServiceProvider.Instance.Projector;
            }
            return __projector;
        }
    }

    public static bool CanBeUsedAsPolarCoordinates(double lat, double lon)
    {
        return CanBeUsedAsLatitude(lat) && CanBeUsedAsLongitude(lon);
    }
    public static bool CanBeUsedAsLatitude(double lat)
    {
        if (lat == double.NaN)
            return false;
        if (lat < -90)
            return false;
        if (lat > 90)
            return false;
        return true;
    }
    public static bool CanBeUsedAsLongitude(double lon)
    {
        if (lon == double.NaN)
            return false;
        if (lon < -180)
            return false;
        if (lon > 180)
            return false;
        return true;
    }

    #region static transformations ...ToPolar

    public static PolarCoordinates UnitySpaceToPolar(Vector3 UnitySpacePosition)
    {
        
        // todo: replace any occurance of ToPolar with this (if possible).
        // todo: this is only float-precision.
        return new PolarCoordinates(
            Projector.ToPolar(UnitySpacePosition.x + CenterOfUnitySpace.CenterMapSpace.x, UnitySpacePosition.z + CenterOfUnitySpace.CenterMapSpace.z)
            );
        
    }
    public static PolarCoordinates MapSpaceToPolar()
    {
        throw new System.NotImplementedException();
        //return new PolarCoordinates(Projector.ToPolar(CenterOfUnitySpace.CenterMapSpace.x, CenterOfUnitySpace.CenterMapSpace.z));
    }

    [Obsolete("Try calling this with a Vector3d instead of Vector3.")]
    public static PolarCoordinates UnitSphereToPolar(Vector3 coordinatesOnUnitSphere)
    {
        return UnitSphereToPolar(new Vector3d(coordinatesOnUnitSphere));
    }
    public static PolarCoordinates UnitSphereToPolar(Vector3d coordinatesOnUnitSphere)
    {

        Debug.Assert(coordinatesOnUnitSphere.sqrMagnitude - 1f < 0.001, "lengths should be 1");
        // North- and South-pole.
        if (coordinatesOnUnitSphere.y == 1)
        {
            return new PolarCoordinates(90, 0);
        }
        if (coordinatesOnUnitSphere.y == -1)
        {
            return new PolarCoordinates(-90, 0);
        }
        double lat = Mathd.Rad2Deg * Math.Asin(coordinatesOnUnitSphere.y);
        double lon = -Mathd.Rad2Deg * Math.Atan2(coordinatesOnUnitSphere.x, coordinatesOnUnitSphere.z);

        return new PolarCoordinates(lat, lon);
    }
    #endregion


    /// <summary>
    /// The Distance between two PolarCoordinates, in Meter.
    /// This will _not_ work well for very distant Coordinates!
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    public static float MapDistanceMeters(PolarCoordinates a, PolarCoordinates b)
    {
        float distance = Vector3.Distance(Projector.PolarToX0Z(a.lat, a.lon), Projector.PolarToX0Z(b.lat, b.lon));
        


        Debug.LogWarning("this way of calculating the distance can cause trouble.");
        
        
        return distance;
    }

    public static float DistanceMeters(PolarCoordinates a, PolarCoordinates b)
    {
        float distance = (float)( EarthRadius_Average * DistanceAngleRadians(a, b));

        return distance;
    }

    
    /// <summary>
    /// The angle between two polar coordinates in Radians.
    /// _Very_ exact on big distances.
    /// Perhaps not very exact on small distances.
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns>Euler!</returns>
    public static double DistanceAngleRadians(PolarCoordinates a, PolarCoordinates b)
    {
        return deg2Rad * Vector3d.Angle(a.PolarToUnitSphere(), b.PolarToUnitSphere());
    }
    /// <summary>
    /// The angle between two polar coordinates in Euler.
    /// _Very_ exact on big distances.
    /// Perhaps not very exact on small distances.
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns>radians</returns>
    public static double DistanceAngleEuler(PolarCoordinates a, PolarCoordinates b)
    {
        return Vector3d.Angle(a.PolarToUnitSphere(), b.PolarToUnitSphere());
    }

    public double lat;
    public double lon;

    private Vector3? _unitySpace;
    public Vector3 UnitySpace
    {
        get
        {
            if (_unitySpace == null)
            {
                _unitySpace = CalculateUnitySpace();
            }
            return (Vector3)_unitySpace;
        }
    }
    private Vector2? _mapSpace;
    public Vector2 MapSpace //Todo: take a look if this is actually needed.
    {
        get
        {
            if (_mapSpace == null)
            {
                _mapSpace = CalculateMapSpace();
            }
            return (Vector3)_mapSpace;
        }
    }

    #region constructors

    public PolarCoordinates(double lat, double lon)
    {
        
        this.lat = lat;
        this.lon = lon;


        //Debug.Assert(this.lat > this.lon, "lat<lon, this is bullshit for Europe!\n" + this.lat + " < " + this.lon);
    }

    

    public PolarCoordinates(float lat, float lon)
    {
        this.lat = lat;
        this.lon = lon;
        //Debug.Assert(this.lat > this.lon, "lat<lon, this is bullshit for Europe!");
    }
    /// <summary>
    /// Note: EPSG:4326 specifically states that the coordinate order should be latitude, longitude.
    /// </summary>
    /// <param name="polarVector">EPSG:4326 specifically states that the coordinate order should be latitude, longitude.</param>
    public PolarCoordinates(Vector2 polarVector)
    {
        this.lat = polarVector.x;
        this.lon = polarVector.y;
        //Debug.Assert(this.lat > this.lon, "lat<lon, this is bullshit for Europe!");
    }
    #endregion

    #region transformations PolarTo...
    private Vector3 CalculateUnitySpace()
    {//Debug.Log("PolarToUnitySpace: lat = " + lat + ", lon = " + lon + ". Will return " + Projector.PolarToX0Z(lat, lon) + " - " + CenterOfUnitySpace.CenterMapSpace);
        
        Vector3 result = Projector.PolarToX0Z(lat, lon) - CenterOfUnitySpace.CenterMapSpace;
        //Debug.Assert(result.magnitude < 50000, "Now, this seems to be out a bit far away. the Unity-coordinates seem to be " + result + ", which is more than 50 km from the center.");
        return result;
        
    }
    private Vector2 CalculateMapSpace()
    {
        return new Vector2((float)Projector.LatToMapX(lat), (float)Projector.LonToMapY(lon));
    }

    //public Vector3 PolarToUnitSphere()
    //{
    //    Vector3 result = new Vector3();
    //    result.y = Mathf.Sin(Mathf.Deg2Rad * (float)lat);
    //    result.x = Mathf.Sin(Mathf.Deg2Rad * -(float)lon) * Mathf.Cos(Mathf.Deg2Rad * (float)lat);
    //    result.z = Mathf.Cos(Mathf.Deg2Rad * -(float)lon) * Mathf.Cos(Mathf.Deg2Rad * (float)lat);
    //    Debug.Assert(result.magnitude - 1f < 0.0001f, "There was an error with translating Polar coordinates to Cartasian coordinages!");
    //    return result;
    //}

    public Vector3d PolarToUnitSphere()
    {
        Vector3d result = new Vector3d();
        result.y = Mathd.Sin(deg2Rad * lat);
        result.x = Mathd.Sin(deg2Rad * -lon) * Mathd.Cos(deg2Rad * lat);
        result.z = Mathd.Cos(deg2Rad * -lon) * Mathd.Cos(deg2Rad * lat);
        Debug.Assert(result.magnitude - 1f < 0.0001, "There was an error with translating Polar coordinates to Cartasian coordinages!");
        Debug.Assert(Math.Sign(lat) == Math.Sign(result.y), "there is a fundamental error during conversion to unit-sphere.");
        return result;
    }
    

    #endregion
    public override string ToString()
    {
        return "PolarCoordinates("+lat.ToString("F6",System.Globalization.CultureInfo.InvariantCulture)+","+lon.ToString("F6", System.Globalization.CultureInfo.InvariantCulture) + ")";
    }
}
