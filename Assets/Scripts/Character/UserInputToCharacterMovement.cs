﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserInputToCharacterMovement : MonoBehaviour {
    //static int wallLayer = 9;
    Animator animator;
    int isWalkingHash = Animator.StringToHash("isWalking");
    Vector3 target;
    bool isOnTarget = true;
    [SerializeField]
    Collider floor;
	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        GetMouseInput();
        WalkToTarget();
	}
    private void GetMouseInput()
    {
        if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            floor.Raycast(ray, out hitInfo, Mathf.Infinity);
            target = hitInfo.point;
            Debug.Assert(target.y < 0.5f && target.y > -0.5f, "target likely to high or low!");

            isOnTarget = false;
        }
    }
    private void WalkToTarget()
    {
        if (isOnTarget)
        {
            return;
        }
        if(Vector3.Distance(transform.position, target) < 0.2)
        {
            animator.SetBool(isWalkingHash, false); //perhaps this does not need a float.
            isOnTarget = true;
            return;
        }
        this.transform.LookAt(target, Vector3.up);
        animator.SetBool(isWalkingHash, true);
    }
}
