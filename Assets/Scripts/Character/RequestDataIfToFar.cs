﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Deserialisation;


public class RequestDataIfToFar : MonoBehaviour {

    [SerializeField]
    RequestRailroad requestRailroad;
    void Start () {
        Debug.Assert(requestRailroad != null);
	}

    private int updateCount = 0;
	void Update ()
    {
        updateCount = (updateCount + 1) % 500;

        if(updateCount == 50)
        {
            // Todo: this should be done by an app-state or any more sensible variable
            if(CenterOfUnitySpace.hasBeenSet)
                requestRailroad.RequestNewMapIfNecessary(PolarCoordinates.UnitySpaceToPolar(this.transform.position));
        }
        
	}

}
