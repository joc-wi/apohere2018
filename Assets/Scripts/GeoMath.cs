﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class GeoMath
{
    public const double earthRadius = 6371000;
    public static Vector2d UnitSphereToPolar(Vector3 coordinatesOnUnitSphere)
    {
        Debug.LogWarning("We want to get rid of GeoMath.UnitSpereToPolar and use PolarCoordinates.UnitSphereToPolar instead!");
        Debug.Assert(coordinatesOnUnitSphere.sqrMagnitude - 1f < 0.001, "lengths should be 1");
        // North- and South-pole.
        if (coordinatesOnUnitSphere.y == 1)
        {
            return new Vector3d(90, 0);
        }
        if (coordinatesOnUnitSphere.y == -1)
        {
            return new Vector3d(-90, 0);
        }
        double lat = Mathd.Rad2Deg * Mathd.Asin(coordinatesOnUnitSphere.y);
        double lon = -Mathd.Rad2Deg * Mathd.Atan2(coordinatesOnUnitSphere.x, coordinatesOnUnitSphere.z);

        return new Vector2d(lat, lon);
    }

    /// <summary>
    /// Careful! this was written to late in the evening, and should be double-checked!
    /// </summary>
    /// <param name="polarCoordinates"></param>
    /// <param name="radius"></param>
    /// <returns></returns>
    public static Vector3d PolarToCartasian(Vector2d polarCoordinates, float radius = 1f)
    {
        Debug.LogWarning("This is in use? Check if it could be changed to take the polarCoordinates using the PolarCoordinates-class.");
        Vector3d result = new Vector3d();
        result.y = Mathd.Sin(Mathd.Deg2Rad * polarCoordinates.x) * radius;
        result.x = Mathd.Sin(Mathd.Deg2Rad * -polarCoordinates.y) * Mathd.Cos(Mathd.Deg2Rad * polarCoordinates.x) * radius;
        result.z = Mathd.Cos(Mathd.Deg2Rad * -polarCoordinates.y) * Mathd.Cos(Mathd.Deg2Rad * polarCoordinates.x) * radius;
        Debug.Assert(result.magnitude - radius < 0.0001f, "There was an error with translating Polar coordinates to Cartasian coordinages!");
        return result;
    }

    

    
    public static float DistanceToLineSegment(PolarCoordinates point, PolarCoordinates lineStart, PolarCoordinates lineEnd)
    {
        float ps = PolarCoordinates.DistanceMeters(point, lineStart);
        float pe = PolarCoordinates.DistanceMeters(point, lineEnd);
        float se = PolarCoordinates.DistanceMeters(lineStart, lineEnd);

        // If the footpoint of p on the line se is _outside_ of the segment between s and e,
        // the angle of the triangle at s or e is bigger 90°.
        // For exactly 90°, pytagoras would say: ps²+se²=pe² or ps²+se²=pe².
        // For _over_ 90°, it will be either ps²+se²<pe² or ps²+se²<pe².

        if(ps*ps + se*se < pe*pe) //footpoint before start of segment
        {
            Debug.Assert(pe > ps);
            return ps;
        }
        if(pe*pe + se*se < ps * ps) //footpoint behind end of segment
        {
            Debug.Assert(ps > pe);
            return pe;
        }

        // Ok, now we use the formular on the following page to get the distance:
        // http://www.mathematische-basteleien.de/hoehen.htm
        // Seems like it is based on the "Formula of Heron". Quite cool, actually.

        float s = 0.5f * (ps + pe + se); // half of circumference.
        return (2 / se) * Mathf.Sqrt(s * (s - ps) * (s - pe) * (s - se));
    }


    public static double EulerAngleToLineSegment(PolarCoordinates point, PolarCoordinates lineStart, PolarCoordinates lineEnd)
    {
        double ps = PolarCoordinates.DistanceAngleEuler(point, lineStart);
        double pe = PolarCoordinates.DistanceAngleEuler(point, lineEnd);
        double se = PolarCoordinates.DistanceAngleEuler(lineStart, lineEnd);

        // If the footpoint of p on the line se is _outside_ of the segment between s and e,
        // the angle of the triangle at s or e is bigger 90°.
        // For exactly 90°, pytagoras would say: ps²+se²=pe² or ps²+se²=pe².
        // For _over_ 90°, it will be either ps²+se²<pe² or ps²+se²<pe².

        if (ps * ps + se * se < pe * pe) //footpoint before start of segment
        {
            Debug.Assert(pe > ps);
            return ps;
        }
        if (pe * pe + se * se < ps * ps) //footpoint behind end of segment
        {
            Debug.Assert(ps > pe);
            return pe;
        }

        // Ok, now we use the formular on the following page to get the distance:
        // http://www.mathematische-basteleien.de/hoehen.htm
        // Seems like it is based on the "Formula of Heron". Quite cool, actually.

        double s = 0.5f * (ps + pe + se); // half of circumference.
        return (2 / se) * Mathd.Sqrt(s * (s - ps) * (s - pe) * (s - se));
    }
}

