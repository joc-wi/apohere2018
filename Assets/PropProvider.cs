﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WorldToHexagons;

namespace GeometyCreation
{
    public class PropProvider : MonoBehaviour
    {

        [SerializeField] private List<GameObject> groundPlantPrefabs = new List<GameObject>();
        [SerializeField] private List<GameObject> waterPlantPrefabs = new List<GameObject>();

        internal GameObject GetGroundPlant(HexCenterPoint hex)
        {
            return Instantiate(groundPlantPrefabs[Random.Range(0, groundPlantPrefabs.Count)]);
        }
        internal GameObject GetWaterPlant(HexCenterPoint hex)
        {
            return Instantiate(waterPlantPrefabs[Random.Range(0, waterPlantPrefabs.Count)]);
        }
    }
}